﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsChecker : MonoBehaviour {

    [SerializeField] private List<string> ToCheckTags;
    [SerializeField] private GameObject Checker;

    private IElementsChecker checker;


    private void Awake() {
        checker = Checker.GetComponent<IElementsChecker>();
    }


    private void OnTriggerEnter(Collider other) {
        Transform tr = other.transform;
        if (tr.Equals(transform) || tr.Equals(transform.parent))
            return;
        if (ToCheckTags.Contains(other.tag)) {
            //Debug.Log(transform.parent+" , IN: " + other);
            checker.ManageColliderEntered(other);
        }
    }

    private void OnTriggerExit(Collider other) {
        Transform tr = other.transform;
        if (tr.Equals(transform) || tr.Equals(transform.parent))
            return;
        if (ToCheckTags.Contains(other.tag)) {
            //Debug.Log(transform.parent + " , OUT: " + other);
            checker.ManageColliderExited(other);
        }
    }




    public interface IElementsChecker {
        void ManageColliderEntered(Collider collider);
        void ManageColliderExited(Collider collider);
    }

}

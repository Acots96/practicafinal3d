﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosswalk : MonoBehaviour {

    public TrafficLight TrafficLight;

    public bool IsTLGreen { get => TrafficLight.IsGreen; }


    private void OnDrawGizmos() {
        if (TrafficLight) {
            Gizmos.color = Color.green;
            Vector3 dir = TrafficLight.transform.position - transform.position;
            Gizmos.DrawRay(transform.position, dir * 0.95f);
            Gizmos.DrawSphere(transform.position + dir * 0.95f, 0.3f);
        }
    }

}

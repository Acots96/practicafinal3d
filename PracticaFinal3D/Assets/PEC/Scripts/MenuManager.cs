﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuManager : MonoBehaviour {

    [SerializeField] private CanvasGroup Canvas;
    [SerializeField] private Image BackgroundPanel;
    [SerializeField] private GameObject LoadingPanel;
    [SerializeField] private float FadeTime;

    [SerializeField] private Menu MainMenu, SettingsMenu;
    [SerializeField] private List<string> ResolutionsList;

    [SerializeField] private AudioMixer GameMixer;
    [SerializeField] private AudioSource EffectSound;

    public static List<string> Resolutions { get => Instance.ResolutionsList; }

    private static MenuManager Instance;


    private void Awake() {
        if (Instance) {
            Destroy(Instance);
            Instance = null;
        }
        Instance = this;
        DisableInput();
        CheckResolution();
        StartBackground();
    }


    // metodo que comprueba la resolución de la pantalla para saber
    // cual es la máxima resolucion que el jugador puede elegir
    private void CheckResolution() {
        Debug.Log(Screen.currentResolution);
        Screen.fullScreen = true;
        Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
        List<string> finalResolutions = new List<string>();
        foreach (string res in ResolutionsList) {
            string[] splited = res.Split('x');
            int width = int.Parse(splited[0]),
                height = int.Parse(splited[1]);
            if (width < Screen.currentResolution.width && height < Screen.currentResolution.height)
                finalResolutions.Add(res);
        }
        ResolutionsList = finalResolutions;
    }


    // en lugar de mostrar el menu de golpe, primero se hace un efecto fade
    // de negro a transparente mediante un panel delante del menu,
    // luego aparecen paulatinamente los 3 botones principales
    private void StartBackground() {
        Color c1 = BackgroundPanel.color;
        c1.r = 0;
        c1.g = 0;
        c1.b = 0;
        UnityEvent evt = new UnityEvent();
        evt.AddListener(EnableInput);
        UnityEvent evtBtns = new UnityEvent();
        evtBtns.AddListener(delegate { ShowButtons(true, evt); });
        StartCoroutine(SimpleColorTransitionEffect(c1, BackgroundPanel.color, BackgroundPanel, FadeTime, evtBtns));
    }

    private void ShowButtons(bool show, UnityEvent evt = null) {
        MainMenu.ShowButtons(show, evt);
        StartCoroutine(BackgroundColorEffect());
    }



    public static void EnableInput() {
        Instance.Canvas.blocksRaycasts = true;
    }
    public static void DisableInput() {
        Instance.Canvas.blocksRaycasts = false;
    }


    public void SetMusicVolume(float vol) {
        GameMixer.SetFloat("MusicVolume", vol);
    }
    public void SetEffectsVolume(float vol) {
        if (!EffectSound.isPlaying)
            EffectSound.Play();
        GameMixer.SetFloat("EffectsVolume", vol);
    }



    // metodo para iniciar el efecto fade de inicio del
    // juego (transparente-negro) y cerrar los submenus abiertos
    public void StartGame() {
        DisableInput();
        UnityEvent evt = new UnityEvent();
        evt.AddListener(LoadCityScene);
        MainMenu.Close(evt);        
    }
    public void LoadCityScene() {
        Color c1 = BackgroundPanel.color;
        c1.r = 0;
        c1.g = 0;
        c1.b = 0;
        UnityEvent evt = new UnityEvent();
        evt.AddListener(delegate {
            BackgroundPanel.gameObject.SetActive(false);
            MainMenu.gameObject.SetActive(false);
            SettingsMenu.gameObject.SetActive(false);
            LoadingPanel.SetActive(true);
            SceneManager.LoadScene("City");
        });
        StartCoroutine(SimpleColorTransitionEffect(BackgroundPanel.color, c1, BackgroundPanel, FadeTime, evt));
        //SceneManager.LoadScene("City");
    }

    public void QuitGame() {
        Application.Quit();
    }


    // efecto fade de color con evento al final
    private IEnumerator SimpleColorTransitionEffect(Color startColor, Color targetColor, Image image, float time, UnityEvent evt) {
        float progress = 0;
        while (progress < 1) {
            progress += Time.deltaTime / time;
            image.color = Color.Lerp(startColor, targetColor, progress);
            yield return null;
        }
        //yield return new WaitForSeconds(0.5f);
        evt?.Invoke();
    }



    // efecto sencillo y apenas visible sobre la imagen de fondo
    // para que vaya cambiando sutilmente de color
    private IEnumerator BackgroundColorEffect() {
        float time = FadeTime * 1f;
        float min = 0.8f;
        List<Color> colors = new List<Color> {
            new Color(1f, 1f, min),
            new Color(1f, min, min),
            new Color(1f, min, 1f),
            new Color(min, min, 1f),
            new Color(min, 1f, 1f),
            new Color(min, 1f, min)
        };
        int colorIdx = 0;
        Scene scene = SceneManager.GetActiveScene();
        while (scene.Equals(SceneManager.GetActiveScene())) {
            float progress = 0;
            Color actual = colors[colorIdx++];
            if (colorIdx == colors.Count)
                colorIdx = 0;
            Color next = colors[colorIdx];
            while (progress < 1) {
                progress += Time.deltaTime / time;
                BackgroundPanel.color = Color.Lerp(actual, next, progress);
                yield return null;
            }
        }
    }

}

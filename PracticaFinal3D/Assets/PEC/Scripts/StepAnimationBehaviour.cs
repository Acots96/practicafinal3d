﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepAnimationBehaviour : StateMachineBehaviour {

    [SerializeField] private bool IsRunning; //walking otherwise

    private int step;
    private Actor actor;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        actor = animator.GetComponent<Actor>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float t = stateInfo.normalizedTime;
        t -= (int)t;
        if (IsRunning) {
            if (t > 0.35f && t < 0.4f && step == 0) {
                step++;
                actor.PlayStepSound(0);
            } else if (t > 0.85f && t < 0.9f && step == 1) {
                step--;
                actor.PlayStepSound(1);
            }
        } else {
            if (t > 0.35f && t < 0.4f && step == 0) {
                step++;
                actor.PlayStepSound(0);
            } else if (t > 0.85f && t < 0.9f && step == 1) {
                step--;
                actor.PlayStepSound(1);
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}

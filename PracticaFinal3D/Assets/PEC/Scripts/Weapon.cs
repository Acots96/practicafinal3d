﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour {

    public GameManager.WeaponType Type;
    public float Damage;

    public abstract void Attack(Vector3 origin, Vector3 direction);

}

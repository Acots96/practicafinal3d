﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehiclesSpawner : MonoBehaviour {

    [SerializeField] private bool AllowVehiclesCreation;
    [SerializeField] private List<GameObject> VehiclesPrefabs;
    [SerializeField] private Transform VehiclesParent, TLGroupsParent;
    [SerializeField] private int MaxVehicles;

    private int vehiclesCount;


    private void Start() {
        if (AllowVehiclesCreation) {
            List<Transform> finishNodes = new List<Transform>();
            foreach (Transform tlGroup in TLGroupsParent) {
                foreach (Transform fn in tlGroup.Find("FinishNodes"))
                    finishNodes.Add(fn);
            }
            for (int i = 0; i < MaxVehicles; i++) {
                Transform fn = finishNodes[Random.Range(0, finishNodes.Count)];
                finishNodes.Remove(fn);
                InstantiateVehicleWithNode(fn, fn.GetComponent<Node>());
            }
        }
    }


    // los vehiculos se instancian en los FinishNodes y se les asigna
    // como destino uno de los StartNodes adyacentes
    private void InstantiateVehicleWithNode(Transform posAndRot, Node node) {
        GameObject randomVehicle = VehiclesPrefabs[Random.Range(0, VehiclesPrefabs.Count)];
        VehicleController vehicle = Instantiate(randomVehicle, posAndRot.position, posAndRot.rotation).GetComponent<VehicleController>();
        vehicle.SetNextNode(node);
        vehicle.transform.SetParent(VehiclesParent);
        vehicle.gameObject.name += vehiclesCount++ + "";
    }
}

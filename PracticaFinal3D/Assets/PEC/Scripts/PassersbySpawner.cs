﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassersbySpawner : MonoBehaviour {

    [SerializeField] private bool AllowPassersbyCreation;
    [SerializeField] private Transform PassersbyNodesParent;
    [SerializeField] private List<GameObject> PassersbyPrefabs;
    [SerializeField] private Transform PassersbyParent;
    [SerializeField] private int MaxPassersby;

    private List<Transform> passersbyNodesGroupsTr;
    private int passersbyCount;


    private void Awake() {
        passersbyNodesGroupsTr = new List<Transform>();
        foreach (Transform t in PassersbyNodesParent) {
            passersbyNodesGroupsTr.Add(t);
        }
    }

    private void Start() {
        if (AllowPassersbyCreation)
            SpawnPassersby();
    }


    // peatones instanciados en nodos al azar, teniendo
    // siempre en cuenta la estructura de GOs en Hierarchy
    private void SpawnPassersby() {
        List<Transform> nodes = new List<Transform>();
        foreach (Transform t in passersbyNodesGroupsTr) {
            if (t.childCount > 0) {
                foreach (Transform n in t)
                    nodes.Add(n);
            } else {
                nodes.Add(t);
            }
        }
        List<Transform> auxNodes = new List<Transform>(nodes);
        for (int i = 0; i < MaxPassersby; i++) {
            if (auxNodes.Count == 0)
                auxNodes = new List<Transform>(nodes);
            Transform n = auxNodes[Random.Range(0, auxNodes.Count)];
            auxNodes.Remove(n);
            InstantiatePasserbyWithNode(n, n.GetComponent<Node>());
        }
    }

    // instancia un peaton con un nodo
    private void InstantiatePasserbyWithNode(Transform posAndRot, Node node) {
        GameObject randomPasserby = PassersbyPrefabs[Random.Range(0, PassersbyPrefabs.Count)];
        //PasserbyController passerby = Instantiate(randomPasserby, posAndRot.position, posAndRot.rotation).GetComponent<PasserbyController>();
        PasserbyController passerby = Instantiate(randomPasserby, posAndRot.position, Quaternion.identity).GetComponent<PasserbyController>();
        passerby.SetNextNode(node);
        passerby.transform.SetParent(PassersbyParent);
        passerby.gameObject.name += passersbyCount++ + "";
        GameManager.AddActor(passerby.gameObject);
    }


    // metodo llamado cuando un peaton consigue escapar de los zombies
    // que le perseguian, para encontrar el nodo mas cercano y retomar
    // el comportamiento de wander
    public Node FindClosestPasserbyNode(Vector3 pos) {
        float dist = float.MaxValue;
        Transform closestGroup = passersbyNodesGroupsTr[0];
        foreach (Transform t in passersbyNodesGroupsTr) {
            float d = Vector3.Distance(t.position, pos);
            if (d < dist) {
                dist = d;
                closestGroup = t;
            }
        }
        if (closestGroup.childCount > 0) {
            float distN = float.MaxValue;
            Transform closestNode = closestGroup.GetChild(0);
            foreach (Transform n in closestGroup) {
                float d = Vector3.Distance(n.position, pos);
                if (d < distN) {
                    distN = d;
                    closestNode = n;
                }
            }
            return closestNode.GetComponent<Node>();
        } else {
            return closestGroup.GetComponent<Node>();
        }
    }


    // metodo llamado desde PasserbyController al morir,
    // para instanciar otro peaton aleatoriamente
    public void InstantiateRandomPasserby() {
        Transform groupTr = passersbyNodesGroupsTr[Random.Range(0, passersbyNodesGroupsTr.Count)];
        Transform nodeTr = groupTr.GetChild(Random.Range(0, groupTr.childCount));
        InstantiatePasserbyWithNode(nodeTr, nodeTr.GetComponent<Node>());
        /*GameObject passerbyGO =
            Instantiate(PassersbyPrefabs[Random.Range(0, PassersbyPrefabs.Count)], nodeTr.position, Quaternion.identity);
        passerbyGO.GetComponent<PasserbyController>().SetNextNode(nodeTr.GetComponent<Node>());
        passerbyGO.transform.SetParent(PassersbyParent);
        passerbyGO.name += passersbyCount++ + "";
        GameManager.AddActor(passerbyGO);*/
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour {

    [Tooltip("FirstTransitionNodes dont have NextNode")]
    [SerializeField] private List<Node> NextNodes;
    [SerializeField] private List<Node> LastTransitionNodes;

    private Transform tr;


    private void Start() {
        tr = transform;
        //
        // cada TransitionFirstNode genera tantas transiciones
        // como TransitionLastNode tenga
        if (LastTransitionNodes != null && LastTransitionNodes.Count > 0) {
            LastTransitionNodes.RemoveAll(n => n == null);
            foreach (Node n in LastTransitionNodes)
                GenerateTransition(n, GameManager.TransitionsNodesPerMeter);
        }
    }


    public Node GetRandomNextNode() {
        return NextNodes[Random.Range(0, NextNodes.Count)];
    }

    public Node GetRandomNextNodeExcept(params Node[] nodes) {
        List<Node> aux = new List<Node>(NextNodes);
        foreach (Node n in nodes)
            aux.Remove(n);
        return aux[Random.Range(0, aux.Count)];
    }


    public void AddNext(Node n) {
        if (NextNodes == null)
            NextNodes = new List<Node>();
        //NextNodes.Clear();
        NextNodes.Add(n);
    }


    public Node[] GetNextNodes() {
        return NextNodes.ToArray();
    }
    
    

    // algoritmo para generar los nodos de transicion que permiten simular
    // el efecto de giro suave de los coches en los cruces
    private void GenerateTransition(Node lastNode, float NodesPerMeter) {
        // si la futura direccion es la misma, porque el vehiculo sigue recto 
        // entonces no hace falta generar una transicion de nodos para rotar
        if (tr.forward == lastNode.transform.forward) {
            AddNext(lastNode);
            return;
        }
        // partiendo de que la rotacion sera de (+-)90 grados, se busca el punto 
        // central de dicha rotacion desde el espacio local del TransitionFirstNode
        Vector3 firstPos = tr.position;
        Vector3 lastPos = lastNode.transform.position;
        Vector3 rotationCenter = firstPos;
        float X = tr.InverseTransformPoint(lastPos).x;
        rotationCenter += X * tr.right;
        float sign = Mathf.Sign(X);
        //
        // se obtienen las distancias TransitionFirstNode-Centro y TransitionLastNode-Centro
        float firstDist = Vector3.Distance(rotationCenter, firstPos);
        float lastDist = Vector3.Distance(rotationCenter, lastPos);
        //
        // se obtiene la cantidad de nodos que tendra la transicion 
        // con el incremento desde el TransitionFirstNode
        int nodesAmount = (int) (Vector3.Distance(firstPos, lastPos) * NodesPerMeter);
        float angleIncr = 90 / nodesAmount;
        float distIncr = (lastDist - firstDist) / nodesAmount;
        Quaternion startRot = lastNode.transform.rotation * Quaternion.AngleAxis(180, Vector3.up);
        //
        Node previous = this;
        for (int i = 1; i < nodesAmount - 1; i++) {
            // cada nodo de transicion se pone en una posicion y rotacion concretas
            // para que el vehiculo haga la "interpolacion" entre TFN y TLN
            // y simule asi el giro
            Quaternion rot = startRot * Quaternion.AngleAxis(angleIncr * i * sign, Vector3.up);
            Vector3 pos = rotationCenter + rot * Vector3.forward * (firstDist + distIncr * (i-1));
            Transform newNodeTr = new GameObject("TransitionNode" + i).transform;
            newNodeTr.SetPositionAndRotation(pos, rot * Quaternion.AngleAxis(90 * sign, Vector3.up));
            newNodeTr.SetParent(tr);
            Node node = newNodeTr.gameObject.AddComponent<Node>();
            previous.AddNext(node);
            previous = node;
        }
        previous.AddNext(lastNode);
    }

    

    // para pintar todas las transiciones entre nodos
    private void OnDrawGizmos() {
        //
        Vector3 start = transform.position;
        if (NextNodes != null) {
            Gizmos.color = name.StartsWith("Passerby") ? Color.magenta : Color.cyan;
            foreach (Node n in NextNodes) {
                if (n == null)
                    continue;
                Vector3 dir = n.transform.position - start;
                Gizmos.DrawRay(start, dir * 0.95f);
                Gizmos.DrawSphere(start + dir * 0.95f, 0.3f);
            }
        }
        if (LastTransitionNodes != null) {
            Gizmos.color = Color.yellow;
            foreach (Node n in LastTransitionNodes) {
                if (n == null)
                    continue;
                Vector3 dir = n.transform.position - start;
                Gizmos.DrawRay(start, dir * 0.95f);
                Gizmos.DrawSphere(start + dir * 0.95f, 0.3f);
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : Actor {

    private enum State { Wander, Chase, Attack, GetHit, Dead }

    public bool IsStrong;

    [SerializeField] private float WalkSpeed, RunSpeed, DetectionDistance, StopDistance, Damage, GetHitAnimationTime;
    [SerializeField] private float DetectingTimeFrequency;
    [SerializeField] private LayerMask LayersToDetect;
    [SerializeField, Range(0.01f, 1f)] private float TurnRoughness;
    [SerializeField] private Collider ArmCollider;
    [SerializeField] private GameObject MeshGO;

    [SerializeField] private List<GameObject> ItemsPrefabsToDrop;

    [SerializeField] private AudioSource[] GrowlSounds, AttackGrowlSounds;
    [SerializeField] private AudioSource AttackHitSound;

    private Transform tr;
    private Animator anim;
    private Rigidbody rb;
    private CapsuleCollider col;

    private PECController player;

    private float walkAnimation, speed, targetSpeed, distance, detectingTimeFreq;
    private Vector3 targetDir;
    private bool alreadyHit, chasing, playerDead, isAttackingPasserby;

    private State state;


    private void Awake() {
        tr = transform;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

        /*if (!IsStrong) {
            int r = Random.Range(0, 2);
            anim.SetFloat("WalkAnimation", r);
            WalkSpeed *= r == 0 ? 1.2f : 1f;
        }*/
        targetDir = tr.forward;
        state = State.Wander;
        ArmCollider.enabled = playerDead = false;
        isAttackingPasserby = true;
    }

    private void Start() {
        //player = GameManager.Player;
        //CheckPlayer();
        StartCoroutine(WanderEffect());
        Invoke("GrowlSoundInvoke", Random.Range(2f, 7f));
        Invoke("AttackGrowlSoundInvoke", Random.Range(2f, 7f));
    }


    private void Update() {
        //tr.up = Vector3.up;
        if (state == State.Dead || isAttackingPasserby)
            return;

        // comprueba si ve humanos
        detectingTimeFreq += Time.deltaTime;
        if (detectingTimeFreq >= DetectingTimeFrequency) {
            detectingTimeFreq = 0;
            CheckHumans();
        }
        
        speed = Mathf.Lerp(speed, targetSpeed, 0.1f);

        anim.SetFloat("Speed", speed / RunSpeed);

        //targetDir.y = 0;
        //tr.forward = Vector3.Slerp(tr.forward, targetDir, 0.25f);
    }

    private void FixedUpdate() {
        //agent.destination = tr.position + (speed > 0f ? targetDir : Vector3.zero);
        //rb.velocity = speed > 0 ? agent.velocity : Vector3.zero;
        if (isAttackingPasserby)
            return;
        //
        rb.MovePosition(tr.position + tr.forward * targetSpeed * Time.fixedDeltaTime);
        targetDir.y = 0;
        Quaternion fromTo = Quaternion.FromToRotation(tr.forward, targetDir) * tr.rotation;
        Quaternion q = Quaternion.Slerp(tr.rotation, fromTo, TurnRoughness);
        q = Quaternion.Euler(0, q.eulerAngles.y, 0);
        rb.MoveRotation(q);
    }



    // comprueba los humanos que hay dentro de un radio concreto
    private void CheckHumans() {
        Collider[] cols = Physics.OverlapSphere(tr.position, DetectionDistance, LayersToDetect.value);
        List<Transform> humans = new List<Transform>();
        //
        // si ve al jugador ignora a los demas peatones
        foreach (Collider c in cols) {
            player = c.GetComponent<PECController>();
            if (player)
                break;
            else if (c.GetComponent<PasserbyController>())
                humans.Add(c.transform);
        }
        //
        Actor toChase = null;
        if (player && !player.IsDead) {
            toChase = player;
        } else if (humans.Count > 0) {
            anim.SetBool("IsAttack", false);
            toChase = humans[0].GetComponent<Actor>();
        } else {
            state = State.Wander;
            anim.SetBool("IsAttack", false);
            alreadyHit = false;
            return;
        }
        //
        targetDir = (toChase.transform.position - tr.position).normalized;
        distance = Vector3.Distance(tr.position, toChase.transform.position);

        // attack
        if (distance <= StopDistance) {
            if (state == State.Attack)
                return;
            if (player) {
                alreadyHit = false;
                state = State.Attack;
                targetSpeed = 0;
                anim.SetBool("IsAttack", true);
            } else {
                state = State.Attack;
                targetSpeed = 0;
                anim.SetTrigger("DoAttackPasserby");
                isAttackingPasserby = true;
                toChase.TakeDamage(Damage, gameObject);
            }
        }

        // chase
        else if (distance <= DetectionDistance) {
            if (state == State.Chase || state == State.Attack)
                return;
            Chase();
        }
    }

    private void Chase() {
        alreadyHit = false;
        state = State.Chase;
        chasing = true;
        targetSpeed = RunSpeed;
        anim.SetBool("IsAttack", false);
        Invoke("CountDown", 10f);
    }
    // Para que persiga durante un minimo de 10 segundos, 
    // luego vuelve a wander si el humano esta lejos
    private void CountDown() {
        chasing = false;
    }



    public void CheckArmCollider(float t) {
        ArmCollider.enabled = t > 0.25f && t < 0.85f && !alreadyHit;
        if (t > 0.9f) {
            alreadyHit = false;
            if (distance > StopDistance) {
                Chase();
            }
        }
    }


    public void StandUp() {
        isAttackingPasserby = false;
    }



    /**
     * Bucle en el que hace el movimiento de wander
     * (se mueve un tiempo determinado, se para un tiempo determinado, repite)
     * y espera mientras acontecen los demás estados.
     * El fuerte solo espera.
     */
    private IEnumerator WanderEffect() {
        while (state != State.Dead) {
            yield return new WaitWhile(() => state == State.Attack || state == State.Chase);
            //
            while (state == State.Wander) {
                yield return new WaitWhile(() => isAttackingPasserby);
                if (!IsStrong) {
                    targetSpeed = WalkSpeed;
                    targetDir = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
                    Debug.DrawRay(tr.position, targetDir, Color.cyan);
                    float randTime = Random.value * 7.5f + Time.time + 2f;
                    yield return new WaitWhile(() => randTime > Time.time && state == State.Wander);
                    if (state == State.Wander) {
                        targetSpeed = 0;
                        randTime = Random.value * 5f + Time.time + 2f;
                        yield return new WaitWhile(() => randTime > Time.time && state == State.Wander);
                    }
                } else {
                    targetSpeed = 0;
                    yield return new WaitWhile(() => state == State.Wander);
                }
            }
            yield return new WaitWhile(() => state == State.GetHit);
        }
    }



    public override void TakeDamage(float dmg, GameObject atckr) {
        if (state == State.Dead)
            return;
        if (!IsStrong) {
            state = State.GetHit;
            targetSpeed = 0;
        }
        Life = (int)Mathf.Max(0, Life - dmg);
        if (Life <= 0) {
            state = State.Dead;
            rb.isKinematic = true;
            GetComponent<Collider>().enabled = false;
            DieSound.pitch = Random.Range(0.7f, 1.3f);
            DieSound.Play();
            anim.SetTrigger("DoDie");
            DropItems();
            GameManager.UpdateZombiesKilled(true);
            Destroy(gameObject, 6f);
        } else {
            AudioSource getHit = HitSounds[Random.Range(0, HitSounds.Length)];
            getHit.pitch = Random.Range(0.7f, 1.3f);
            getHit.Play();
            if (!IsStrong) {
                Invoke("GetHitStop", GetHitAnimationTime);
                anim.SetTrigger("DoGetHit");
            }
        }
    }
    
    private void GetHitStop() {
        if (state == State.GetHit) {
            Chase();
        }
    }


    private void DropItems() {
        //drop the key if there is one
        foreach (GameObject item in ItemsPrefabsToDrop) {
            if (item.GetComponent<Item>().Type == Item.ItemType.Key) {
                ItemsPrefabsToDrop.Remove(item);
                Instantiate(item, transform.position, item.transform.rotation);
                break;
            }
        }
        int itemsNum = Random.Range(0, ItemsPrefabsToDrop.Count) + 1;
        for (int i = 0; i < itemsNum; i++) {
            GameObject item = ItemsPrefabsToDrop[Random.Range(0, ItemsPrefabsToDrop.Count)];
            ItemsPrefabsToDrop.Remove(item);
            Instantiate(item, transform.position, item.transform.rotation);
        }
    }


    /**
     * Cada cierto tiempo aleatorio se reproducen los gruñidos,
     * ya sean los de idle o los de atacar.
     */
    private void GrowlSoundInvoke() {
        AudioSource growl = GrowlSounds[Random.Range(0, GrowlSounds.Length)];
        if (state == State.Wander && gameObject.activeSelf) {
            growl.pitch = Random.Range(0.7f, 1.3f);
            growl.Play();
        }
        if (state != State.Dead)
            Invoke("GrowlSoundInvoke", growl.clip.length + Random.Range(5f, 10f));
    }

    private void AttackGrowlSoundInvoke() {
        AudioSource attack = AttackGrowlSounds[Random.Range(0, AttackGrowlSounds.Length)];
        if (state == State.Attack || state == State.Chase) {
            attack.pitch = Random.Range(0.7f, 1.3f);
            attack.Play();
        }
        if (state != State.Dead)
            Invoke("AttackGrowlSoundInvoke", attack.clip.length + Random.Range(3f, 6f));
    }



    public void AddKeyToDrop(GameObject keyPrefab) {
        ItemsPrefabsToDrop.Add(keyPrefab);
    }



    private int stepSoundIdx;

    // pitch distintos segun el paso que sea, para dar la sensacion
    // de alternar entre paso derecho e izquierdo
    public override void PlayStepSound(int i) {
        AudioSource step = StepSounds[stepSoundIdx++ % StepSounds.Length];
        if (i == 0)
            step.pitch = Random.Range(0.7f, 1.1f);
        else
            step.pitch = Random.Range(0.9f, 1.3f);
        step.Play();
    }



    private void OnTriggerEnter(Collider other) {
        PECController player = other.GetComponent<PECController>();
        if (player && !alreadyHit && !playerDead) {
            alreadyHit = true;
            AttackHitSound.pitch = Random.Range(0.7f, 1.3f);
            AttackHitSound.Play();
            player.TakeDamage(Damage * Random.Range(0.7f, 1.3f), gameObject);
        }
    }

    

    // metodo de la explosion.
    // instancia el efecto de particulas de sangre junto a las partes del cuerpo.
    public override void Explode() {
        GameObject explosion = Instantiate(ExplosionEffectPrefab, tr.position, Quaternion.identity);
        AudioSource aS = explosion.GetComponent<AudioSource>();
        aS.pitch = Random.Range(0.85f, 1.15f);
        aS.Play();
        //
        int parts = (int)(Random.Range(0.15f, 0.4f) * ExplosionBodyPartsPrefabs.Count);
        Vector3 center = tr.TransformPoint(col.center);
        for (int i = 0; i < parts; i++) {
            Vector3 pos = center + Vector3.up * Random.Range(-1f, 1f);
            GameObject prefab = ExplosionBodyPartsPrefabs[Random.Range(0, ExplosionBodyPartsPrefabs.Count)];
            ExplosionBodyPartsPrefabs.Remove(prefab);
            GameObject part = Instantiate(prefab, pos, Quaternion.identity);
            //
            Vector3 force = new Vector3(Random.Range(-1f, 1f), Random.value, Random.Range(-1f, 1f)) * 5f;
            part.GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);
            //
            Destroy(part, 6f);
        }
        //
        Destroy(gameObject);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Menu : MonoBehaviour {

    private enum MenuType { Main, Settings, Controls }

    [SerializeField] private MenuType Type;
    [SerializeField] private List<MenuButton> Buttons;
    [SerializeField] private bool DisableAtStart, FadeEffect;

    [HideInInspector] public float FadeTime, DelayBetweenButtons;


    private void Awake() {
        if (DisableAtStart)
            gameObject.SetActive(false);
    }


    /*public void ShowMenu(bool show) {
        gameObject.SetActive(show);
    }*/


    // mostrar el submenu
    public void ShowButtons(bool show, UnityEvent evt = null) {
        if (FadeEffect) {
            for (int i = 0; i < Buttons.Count; i++)
                StartCoroutine(ButtonsAppearEffect(Buttons[i], FadeTime, i * DelayBetweenButtons, show, i == Buttons.Count - 1 ? evt : null));
        } else {
            foreach (MenuButton mb in Buttons)
                mb.gameObject.SetActive(show);
        }
    }

    // metodo para cambiar el focus de boton (mas informacion en MenuButton)
    public void ChangePointerFocus(MenuButton btn) {
        foreach (MenuButton b in Buttons) {
            if (btn == null || !b.Equals(btn)) {
                b.LoseFocus();
            }
        }
    }


    public void Close(UnityEvent evt = null) {
        ChangePointerFocus(null);
        ShowButtons(false, evt);
    }



    // efecto para mostrar los botones de un menu 
    // (usado en los del menu principal)
    private IEnumerator ButtonsAppearEffect(MenuButton mb, float time, float waitToStart, bool show, UnityEvent evt = null) {
        yield return new WaitForSeconds(waitToStart);
        if (show) {
            float progress = 0;
            while (progress < 1) {
                progress += Time.deltaTime / time;
                mb.SetProgress(progress);
                yield return null;
            }
        } else {
            float progress = 1;
            while (progress > 0) {
                progress -= Time.deltaTime / time;
                mb.SetProgress(progress);
                yield return null;
            }
        }
        evt?.Invoke();
    }





    // para mostrar los dos atributos del efecto fade de los botones
    // en el caso de que se active el booleano para este efecto

#if UNITY_EDITOR
    [CustomEditor(typeof(Menu))]
    private class InspectorUpdater : Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            Menu m = target as Menu;
            if (m.FadeEffect) {
                m.FadeTime = EditorGUILayout.FloatField("StartAngleZ", m.FadeTime);
                m.DelayBetweenButtons = EditorGUILayout.FloatField("TargetAngleZ", m.DelayBetweenButtons);
            }
        }
    }
#endif

}

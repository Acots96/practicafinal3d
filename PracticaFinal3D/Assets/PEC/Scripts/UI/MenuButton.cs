﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MenuButton : MonoBehaviour {

    private enum ButtonType { StartGame, Settings, Quit, Resolution, Controls };

    [SerializeField] private Button MButton;
    [SerializeField] private ButtonType Type;
    [SerializeField] private Text MText;
    [SerializeField] private float MaxAlphaButton, MaxAlphaText;
    [SerializeField] private bool HideFromStart, IsDesplegable;

    [HideInInspector] public Menu DesplegableMenu;
    [HideInInspector] public GameObject DesplegableIcon;
    //[HideInInspector] public RectTransform StartPosTr, TargetPosTr;
    [HideInInspector] public float StartAngleZ, TargetAngleZ, TransitionTime;

    [HideInInspector] public GameObject ResolutionsGO;
    [HideInInspector] public Text ResolutionsText;
    [HideInInspector] public GameObject UpdateResolutionButton;

    private Menu menu;
    private Image btnImage, desplegableIconImage;

    private bool selected, desplegated, toggleOn;


    private void Awake() {
        menu = GetComponentInParent<Menu>();
        if (MButton) {
            btnImage = MButton.image;
            MButton.interactable = false;
        }
        if (IsDesplegable) {
            desplegableIconImage = DesplegableIcon.GetComponent<Image>();
            StartCoroutine(DesplegableIconEffect());
        }
        if (HideFromStart)
            SetProgress(0);
    }


    // metodo para el efecto del boton de desplegable
    public void SetProgress(float progress) {
        Color b = btnImage.color;
        b.a = progress * MaxAlphaButton;
        btnImage.color = b;
        Color t = MText.color;
        t.a = progress * MaxAlphaText;
        MText.color = t;
        if (IsDesplegable) {
            Color d = desplegableIconImage.color;
            d.a = progress * MaxAlphaText;
            desplegableIconImage.color = d;
        }
        //if (FrontPanel)
        //FrontPanel.SetActive(progress >= 1);
        MButton.interactable = progress >= 1;
    }


    // metodo para cuando el mouse esta sobre el boton
    public void PointerEnter() {
        if (MButton == null) {
            menu.ChangePointerFocus(this);
        }
        else if (MButton.interactable) {
            MText.color = Color.black;
            menu.ChangePointerFocus(this);
            if (desplegableIconImage) desplegableIconImage.color = Color.black;
            if (IsDesplegable) {
                desplegated = true;
                DesplegableMenu.gameObject.SetActive(true);
            }
        }
    }
    // metodo para cuando el mouse deja de estar sobre el boton
    public void PointerExit() {
        if (MButton == null) {
            MText.color = Color.white;
            return;
        } 
        else if (MButton.interactable) {
            if (IsDesplegable) {
                ColorBlock block = MButton.colors;
                block.disabledColor = block.highlightedColor;
                MButton.colors = block;
                MButton.interactable = false;
            } else {
                MText.color = Color.white;
            }
        }
    }

    private void ChangeColorText() {
        Color c = btnImage.color;
        if (c.b == 0) {
            MText.color = Color.white;
            if (desplegableIconImage) desplegableIconImage.color = Color.white;
        } else {
            MText.color = Color.black;
            if (desplegableIconImage) desplegableIconImage.color = Color.black;
        }
    }


    // metodo para que el boton vuelva a los colores
    // previos al PointerEnter del mouse
    public void LoseFocus() {
        if (IsDesplegable) {
            ColorBlock block = MButton.colors;
            block.disabledColor = block.normalColor;
            MButton.colors = block;
            MText.color = Color.white;
            if (DesplegableMenu) {
                desplegableIconImage.color = Color.white;
                desplegated = false;
                if (DesplegableMenu.gameObject.activeSelf)
                    DesplegableMenu.ChangePointerFocus(null);
                DesplegableMenu.gameObject.SetActive(false);
            }
            if (MButton)
                MButton.interactable = true;
        }
    }


    // metodo para poner el juego en pantalla completa
    public void ToggleButton(Toggle toggle) {
        toggleOn = toggle.isOn;
        ResolutionsGO.SetActive(!toggleOn);
        UpdateResolutionButton.SetActive(Screen.fullScreen != toggleOn);
    }

    // metodo para actualizar el Text que muestra las resoluciones
    // de pantalla, modificado por una de las dos flechas de los lados ("< resolution >")
    public void ChangeResolutionButton(bool isLeft) {
        List<string> resolutions = MenuManager.Resolutions;
        int idx = resolutions.IndexOf(ResolutionsText.text);
        idx += isLeft ? -1 : 1;
        if (idx == resolutions.Count)
            idx = 0;
        else if (idx == -1)
            idx = resolutions.Count - 1;
        ResolutionsText.text = resolutions[idx];
        //
        string[] splited = ResolutionsText.text.Split('x');
        int width = int.Parse(splited[0]),
            height = int.Parse(splited[1]);
        UpdateResolutionButton.SetActive(Screen.width != width || Screen.height != height);
    }

    // metodo para hacer efectivo el cambio de resolucion
    // que ha elegido el usuario mediante un boton que aparece
    // cuando se pone una resolucion diferente a la que ya hay
    public void UpdateResolution() {
        Screen.fullScreen = toggleOn;
        if (toggleOn) {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
        } else {
            Screen.fullScreenMode = FullScreenMode.Windowed;
            string[] splited = ResolutionsText.text.Split('x');
            int width = int.Parse(splited[0]),
                height = int.Parse(splited[1]);
            Screen.SetResolution(width, height, false);
        }
        UpdateResolutionButton.SetActive(false);
    }



    // efecto del icono ">" del boton desplegable
    private IEnumerator DesplegableIconEffect() {
        float multiplier = desplegated ? 1 : -1,
              progress = 0;
        Transform iconTr = DesplegableIcon.transform;
        //Vector3 startPos = StartPosTr.localPosition, 
          //  targetPos = TargetPosTr.localPosition;
        Quaternion startRot = iconTr.rotation, 
            targetRot = Quaternion.AngleAxis(TargetAngleZ - StartAngleZ, Vector3.forward);
        Scene scene = SceneManager.GetActiveScene();
        while (scene.Equals(SceneManager.GetActiveScene())) {
            multiplier = desplegated ? 1 : -1;
            progress += Time.deltaTime / TransitionTime * multiplier;
            progress = Mathf.Max(0, Mathf.Min(1, progress));
            //iconTr.localPosition = Vector3.Lerp(startPos, targetPos, progress);
            iconTr.rotation = Quaternion.Lerp(startRot, targetRot, progress);
            yield return null;
        }
    }





    // (primer if) para mostrar en el inspector todos los atributos
    // relativos al desplegable icon en el caso de haberlo 
    
    // (segundo if) para mostrar en el inspector los atributos 
    // relativos al boton de resolucion

#if UNITY_EDITOR
    [CustomEditor(typeof(MenuButton))]
    private class InspectorUpdater : Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            MenuButton mb = target as MenuButton;
            if (mb.IsDesplegable) {
                if (mb.DesplegableIcon)
                    mb.DesplegableIcon = EditorGUILayout.ObjectField("DesplegableIcon", mb.DesplegableIcon, mb.DesplegableIcon.GetType(), true) as GameObject;
                else
                    mb.DesplegableIcon = EditorGUILayout.ObjectField("DesplegableIcon", null, typeof(GameObject), true) as GameObject;
                if (mb.DesplegableMenu)
                    mb.DesplegableMenu = EditorGUILayout.ObjectField("DesplegableMenu", mb.DesplegableMenu, mb.DesplegableMenu.GetType(), true) as Menu;
                else
                    mb.DesplegableMenu = EditorGUILayout.ObjectField("DesplegableMenu", null, typeof(Menu), true) as Menu;
                //mb.StartPosTr = EditorGUILayout.ObjectField("StartPosTr", mb.StartPosTr, mb.StartPosTr.GetType(), true) as RectTransform;
                //mb.TargetPosTr = EditorGUILayout.ObjectField("TargetPosTr", mb.TargetPosTr, mb.TargetPosTr.GetType(), true) as RectTransform;
                mb.StartAngleZ = EditorGUILayout.FloatField("StartAngleZ", mb.StartAngleZ);
                mb.TargetAngleZ = EditorGUILayout.FloatField("TargetAngleZ", mb.TargetAngleZ);
                mb.TransitionTime = EditorGUILayout.FloatField("TransitionTime", mb.TransitionTime);
            }
            if (mb.Type == ButtonType.Resolution) {
                if (mb.ResolutionsText)
                    mb.ResolutionsText = EditorGUILayout.ObjectField("ResolutionsText", mb.ResolutionsText, mb.ResolutionsText.GetType(), true) as Text;
                else
                    mb.ResolutionsText = EditorGUILayout.ObjectField("ResolutionsText", null, typeof(Text), true) as Text;
                if (mb.ResolutionsGO)
                    mb.ResolutionsGO = EditorGUILayout.ObjectField("ResolutionsGO", mb.ResolutionsGO, mb.ResolutionsGO.GetType(), true) as GameObject;
                else
                    mb.ResolutionsGO = EditorGUILayout.ObjectField("ResolutionsGO", null, typeof(GameObject), true) as GameObject;
                if (mb.UpdateResolutionButton)
                    mb.UpdateResolutionButton = EditorGUILayout.ObjectField("UpdateResolutionButton", mb.UpdateResolutionButton, mb.UpdateResolutionButton.GetType(), true) as GameObject;
                else
                    mb.UpdateResolutionButton = EditorGUILayout.ObjectField("UpdateResolutionButton", null, typeof(GameObject), true) as GameObject;
            }
        }
    }
#endif

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight : MonoBehaviour {

    [SerializeField] private float TimeYellow;  // tiempo que esta en amarillo
    [SerializeField] private Light RedLight, YellowLight, GreenLight;

    public bool IsPasserbyLight;

    public bool IsGreen { get => GreenLight.enabled; }


    public void SetGreen() {
        GreenLight.enabled = false;
        RedLight.enabled = false;
        YellowLight.enabled = true;
        Invoke("GreenAfterYellow", TimeYellow);
    }

    public void SetRed() {
        GreenLight.enabled = false;
        RedLight.enabled = false;
        YellowLight.enabled = true;
        Invoke("RedAfterYellow", TimeYellow);
    }

    private void GreenAfterYellow() {
        YellowLight.enabled = false;
        GreenLight.enabled = true;
    }
    private void RedAfterYellow() {
        YellowLight.enabled = false;
        RedLight.enabled = true;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : Weapon {

    [SerializeField] private Collider SwordCol;
    [SerializeField] private ParticleSystem EnemyImpactEffect;

    [SerializeField] private AudioSource SheatheSound, AttackSound, HitEnemySound, HitSolidSound;

    private Transform tr;
    private Rigidbody rb;
    private Vector3 startLocalPos;
    private List<ZombieController> zombiesHit;
    private bool alreadyHit;


    private void Awake() {
        tr = transform;
        startLocalPos = tr.localPosition;
        rb = GetComponent<Rigidbody>();
        zombiesHit = new List<ZombieController>();
    }

    private void Update() {
        if (gameObject.activeSelf)
            tr.localPosition = startLocalPos;
    }



    public override void Attack(Vector3 origin, Vector3 direction) {
        // nothing
    }
       

    // para activar/desactivar el collider del brazo
    // y resetear el booleano que indica si se ha golpeado
    // al desactivar el collider.
    public void EnableCollider(bool e) {
        if (SwordCol.enabled && !e) {
            alreadyHit = false;
        }
        SwordCol.enabled = e;
    }

    public void ResetHits() {
        zombiesHit.Clear();
    }
    

    public void PlaySheatheSound() {
        if (SheatheSound) {
            SheatheSound.pitch = Random.Range(0.8f, 1.2f);
            SheatheSound.Play();
        }
    }

    public void PlayAttackSound() {
        if (AttackSound) {
            AttackSound.pitch = Random.Range(0.95f, 1.05f);
            AttackSound.Play();
        }
    }


    /**
     * Para evitar que la espada tenga en cuenta más de una vez al mismo
     * zombie (ya que al estar activado su collider, mientras toque el
     * del zombie llamara constantemente al OnTriggerEnter) se utiliza
     * una lista que los tiene en cuenta, asi provocara daño a tantos
     * zombies como la espada toque, pero solo una vez por golpe.
     */
    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<PECController>() != null || other.GetComponentInParent<PECController>() != null)
            return;
        if (AttackSound && AttackSound.isPlaying)
            AttackSound.Stop();
        Actor actor = other.GetComponent<Actor>();
        if (actor) {
            if (actor is ZombieController) {
                ZombieController zombie = actor as ZombieController;
                if (!zombiesHit.Contains(zombie)) {
                    zombiesHit.Add(zombie);
                    zombie.TakeDamage(Damage, gameObject);
                    if (HitEnemySound) {
                        HitEnemySound.pitch = Random.Range(0.8f, 1.2f);
                        HitEnemySound.Play();
                    }
                    GameObject impact = Instantiate(EnemyImpactEffect.gameObject, zombie.transform.position, Quaternion.identity);
                    impact.GetComponent<ParticleSystem>().Play();
                }
            } else if (actor is PasserbyController) {
                GameManager.UpdateZombiesKilled(false);
                actor.Explode();
            }
        } else {
            if (HitSolidSound && !alreadyHit) {
                HitSolidSound.pitch = Random.Range(0.8f, 1.2f);
                HitSolidSound.Play();
                alreadyHit = true;
            }
        }
    }

}

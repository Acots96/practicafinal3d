﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

    public enum WeaponType { Pistol, Shotgun, Sword, None }

    [SerializeField] private Text LifeText, AmmoText, KeysText, HelpText, ZombiesKilledText, InfoText;
    [SerializeField] private GameObject PistolImage, ShotgunImage, SwordImage, 
        PistolTargetIcon, ShotgunTargetIcon, SwordTargetIcon;
    [SerializeField] private Image Panel, InfoPanel;

    [SerializeField] private string GameOverInfo, PressAnyKeyInfo, PressEInfo, LockInfo, StartInfo, 
        WinInfo, KeyFoundInfo, FinishInfo, ShotgunInfo, SwordInfo;

    [SerializeField] private float FadeTime;

    private static GameManager Instance;

    [SerializeField] private PECController controller;
    public static PECController Player { get => Instance.controller; }

    [SerializeField] private Transform ThrillerZone;

    [SerializeField] private AudioSource WalkiTalkieSound;

    private int zombiesKilled;
    private bool finishInfoAlreadyShown;

    [SerializeField] private float transitionsNodesPerMeter;
    public static float TransitionsNodesPerMeter { get => Instance.transitionsNodesPerMeter; }

    [SerializeField] private ZombiesSpawner ZombiesSpawner;
    [SerializeField] private bool LimitActiveActors;
    [SerializeField] private float DisapearDistance;


    private void Awake() {
        if (Instance) {
            Destroy(Instance);
            Instance = null;
        }
        Instance = this;
        StartCoroutine(StartGameEffect());
        Player.CanMove = false;
        actors = new List<GameObject>();
        if (LimitActiveActors)
            InvokeRepeating("CheckActorsPosition", 0.25f, 0.25f);
    }


    // actualiza la informacion del arma que tiene el jugador en ese momento
    public static void UpdateWeaponText(float loadedBullets, float cartridgesBullets) { // municion
        if (loadedBullets == -1)
            Instance.AmmoText.text = "";
        else
            Instance.AmmoText.text = loadedBullets + "/" + cartridgesBullets;
    }
    public static void UpdateWeaponImage(WeaponType type) { // icono
        bool pistol = type == WeaponType.Pistol,
            shotgun = type == WeaponType.Shotgun,
            sword = type == WeaponType.Sword,
            none = type == WeaponType.None;
        Instance.PistolImage.SetActive(pistol);
        Instance.ShotgunImage.SetActive(shotgun);
        Instance.SwordImage.SetActive(sword);
    }
    public static void UpdateWeaponTarget(WeaponType type) { // target (cuando apunta)
        bool pistol = type == WeaponType.Pistol,
            shotgun = type == WeaponType.Shotgun,
            sword = type == WeaponType.Sword,
            none = type == WeaponType.None;
        if (Instance.PistolTargetIcon)
            Instance.PistolTargetIcon.SetActive(pistol && !none);
        if (Instance.ShotgunTargetIcon)
            Instance.ShotgunTargetIcon.SetActive(shotgun && !none);
        if (Instance.SwordTargetIcon)
            Instance.SwordTargetIcon.SetActive(sword && !none);
    }


    public static void UpdateLife(int life) {
        Instance.LifeText.text = life + "";
    }

    public static void UpdateKeys(float keys) {
        Instance.KeysText.text = keys + "";
    }


    // fade de negro a transparente 
    // con sonido de walkie e info
    private IEnumerator StartGameEffect() {
        float progress = 1;
        Color c = Panel.color;
        c.a = progress;
        Panel.color = c;
        yield return new WaitForSeconds(FadeTime * 0.15f);
        while (progress > 0) {
            progress -= Time.deltaTime / (FadeTime * 0.15f);
            c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        Player.CanMove = true;
        if (WalkiTalkieSound)
            WalkiTalkieSound.Play();
        ShowGameInfo(StartInfo);
    }



    public static void Win() {
        Instance.StartCoroutine(Instance.GameOverEffect(Instance.WinInfo));
    }
    public static void GameOver() {
        Instance.StartCoroutine(Instance.GameOverEffect(Instance.GameOverInfo));
    }

    // corutina que hace el fade a negro, comunica al
    // jugador lo que debe hacer (pulsar una tecla)
    // y recarga la escena
    private IEnumerator GameOverEffect(string info) {
        HelpText.text = "";
        float progress = 0;
        while (progress <= 1) {
            progress += Time.deltaTime / FadeTime;
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        HelpText.text = info;
        HelpText.text += "\n\n\n";
        yield return new WaitForSeconds(FadeTime / 2f);
        HelpText.text += PressAnyKeyInfo;
        //
        yield return new WaitUntil(() => Input.anyKeyDown);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }



    public void TeleportToThrillerZone() {
        StartCoroutine(TeleportEffect());
    }

    private IEnumerator TeleportEffect() {
        float progress = 0;
        while (progress <= 1) {
            progress += Time.deltaTime / (FadeTime * 0.15f);
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
        yield return new WaitForSeconds(FadeTime * 0.15f);
        Player.gameObject.SetActive(false);
        Player.transform.position = ThrillerZone.position;
        Player.gameObject.SetActive(true);
        yield return new WaitForSeconds(FadeTime * 0.15f);
        while (progress > 0) {
            progress -= Time.deltaTime / (FadeTime * 0.15f);
            Color c = Panel.color;
            c.a = progress;
            Panel.color = c;
            yield return null;
        }
    }

    public static void ThrillerZombieFinished(Transform tr) {
        tr.gameObject.SetActive(false);
        tr.Rotate(Vector3.up, 180);
        tr.gameObject.SetActive(true);
    }



    public static void ShowEmptyInfo() {
        Instance.HelpText.text = string.Empty;
    }

    public static void ShowPressEInfo() {
        Instance.HelpText.text = Instance.PressEInfo;
    }

    public static void ShowLockInfo() {
        Instance.HelpText.text = Instance.LockInfo;
    }



    public static void UpdateZombiesKilled(bool zombieKilled) {
        Instance.zombiesKilled += zombieKilled ? 1 : -1;
        Instance.zombiesKilled = Mathf.Max(0, Instance.zombiesKilled);
        Instance.ZombiesKilledText.text = Instance.zombiesKilled + "";
    }



    public void ShowGameInfo(string info) {
        StartCoroutine(InfoTextEffect(info));
    }

    // muestra el texto con barra semitransparente que indica
    // al jugador informacion valiosa
    private IEnumerator InfoTextEffect(string t) {
        float progress = 0, fadeTime = 0.5f;
        InfoText.text = t;
        while (progress <= 1) {
            progress = progress += Time.deltaTime / fadeTime;
            Color c = InfoPanel.color;
            c.a = progress * 0.75f;
            InfoPanel.color = c;
            c = InfoText.color;
            c.a = progress;
            InfoText.color = c;
            yield return null;
        }
        yield return new WaitForSeconds(10f);
        while (progress >= 0) {
            progress = progress -= Time.deltaTime / fadeTime;
            Color c = InfoPanel.color;
            c.a = progress * 0.75f;
            InfoPanel.color = c;
            c = InfoText.color;
            c.a = progress;
            InfoText.color = c;
            yield return null;
        }
    }



    public static void EndZone(bool inside) {
        Instance.EndZoneInstance(inside);
    }

    private void EndZoneInstance(bool inside) {
        if (inside) {
            if (!finishInfoAlreadyShown) {
                finishInfoAlreadyShown = true;
                ShowGameInfo(FinishInfo);
            }
            ShowPressEInfo();
        } else {
            ShowEmptyInfo();
        }
    }



    public static void ShowKeyFoundInfo() {
        Instance.ShowGameInfo(Instance.KeyFoundInfo);
    }



    private List<GameObject> actors;

    public static void AddActor(GameObject a) {
        Instance.actors.Add(a);
    }

    // metodo ejecutado 4 veces por segundo para activar/desactivar
    // los actores que esten fuera de un rango respecto al jugador.
    // el objetivo es mejorar la performance del juego
    private void CheckActorsPosition() {
        Vector3 playerPos = Player.transform.position;
        List<int> reallyDeadIdx = new List<int>();
        //
        int idx = 0;
        foreach (GameObject actor in actors) {
            if (!actor) {
                reallyDeadIdx.Add(idx);
                continue;
            }
            float distance = Vector3.Distance(playerPos, actor.transform.position);
            if (distance > DisapearDistance) {
                if (actor.activeSelf)
                    actor.SetActive(false);
            } else if (!actor.activeSelf) {
                actor.SetActive(true);
            }
            idx++;
        }
        // los que hayan sido desactivados se quitan de la lista
        // al final, para no tener que recorrer la lista principal
        // dos veces
        foreach (int i in reallyDeadIdx)
            actors.RemoveAt(i);
    }


    // metodo llamado desde un PasserbyController que ha sido asesinado
    public static void BecomeZombie(PasserbyController passerby, Transform attackerTr = null) {
        Instance.ZombiesSpawner.SpawnRandomZombie(attackerTr);
    }



    // muestra la tecla a pulsar al encontrar una nueva arma.
    public static void ShowNewWeaponInfo(Item.ItemType type) {
        switch (type) {
            case Item.ItemType.WeaponShotgun:
                Instance.ShowGameInfo(Instance.ShotgunInfo);
                break;
            case Item.ItemType.WeaponSword:
                Instance.ShowGameInfo(Instance.SwordInfo);
                break;
        }
    }



    // boton para volver al menu principal
    private void OnGUI() {
        if (GUI.Button(new Rect(10, 10, 100, 50), "EXIT"))
            SceneManager.LoadScene("Menu");
        if (Cursor.visible)
            if (GUI.Button(new Rect(10, 60, 100, 50), "Hide Mouse"))
                Player.HideMouse();
    }

}

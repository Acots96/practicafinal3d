﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLightsGroup : MonoBehaviour {

    [SerializeField] private float TimeBetweenChange;
    [SerializeField] private Transform SenseXParent, SenseZParent;

    private List<TrafficLight> senseX, senseZ;
    private bool isSenseXGreen;


    private void Awake() {
        isSenseXGreen = false;  // empieza siempre por el sentido Z
        InitSenses();
        EnableSense();
    }

    private void InitSenses() {
        senseX = new List<TrafficLight>();
        senseZ = new List<TrafficLight>();
        foreach (Transform t in SenseXParent)
            senseX.Add(t.GetComponent<TrafficLight>());
        foreach (Transform t in SenseZParent)
            senseZ.Add(t.GetComponent<TrafficLight>());
    }


    // metodo "recursivo" que activa/desactiva los semaforos que toque
    // y espera un tiempo concreto para activar/desactivar los 
    // semaforos del sentido contrario
    private void EnableSense() {
        List<TrafficLight> toEnable = isSenseXGreen ? senseX : senseZ,
                          toDisable = isSenseXGreen ? senseZ : senseX;
        isSenseXGreen = !isSenseXGreen;
        //
        foreach (TrafficLight t in toEnable)
            t.SetGreen();
        foreach (TrafficLight t in toDisable)
            t.SetRed();
        //
        Invoke("EnableSense", TimeBetweenChange);
    }

}

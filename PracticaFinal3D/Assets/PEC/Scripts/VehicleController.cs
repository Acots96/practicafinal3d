﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class VehicleController : MonoBehaviour, ElementsChecker.IElementsChecker {

    [SerializeField] private float MaxSpeed, MaxPlayerSpeed, MaxPlayerTurnAngle;
    [SerializeField] private Node TargetNode;
    [SerializeField, Range(0.01f, 1f)] private float TurnRoughness, AccelerationRoughness;
    //[SerializeField] private Transform Checker;
    [SerializeField] private Transform[] Wheels;

    private Transform tr, targetNodeTr;
    private Rigidbody rb;
    private Node crosswalkNode;
    private PECController player;

    private float targetSpeedPct, actualSpeedPct, actualMaxSpeed;
    private bool isWaiting, isPlayerIn;
    private int elementsInFront;
    private Vector3 targetDir, actualDir, frontalWheelsTargetDir, frontalWheelsActualDir;
    private float actualTurnAngle;

    private Collider waitingFor;

    private List<Actor> inFrontZombies;

    [SerializeField] private AudioSource EngineIdleSound, EngineRunSound, CollisionSound, ClaxonSound;
    private float runSoundStartPitch;


    private void Awake() {
        tr = transform;
        rb = GetComponent<Rigidbody>();
        targetSpeedPct = 1;
        elementsInFront = 0;
        actualMaxSpeed = MaxSpeed;
        inFrontZombies = new List<Actor>();
        if (EngineRunSound)
            runSoundStartPitch = EngineRunSound.pitch;
    }

    private void Start() {
        if (!TargetNode)
            return;
        targetNodeTr = TargetNode.transform;
        targetDir = (targetNodeTr.position - tr.position).normalized;
    }


    private void Update() {
        if (isPlayerIn) {
            if (Input.GetKeyDown(KeyCode.E)) {  // salir
                PlayerOut();
            } else if (Input.GetKeyDown(KeyCode.Space)) {  // claxon
                ClaxonSound.Play();
            } else { 
                // control del movimiento del jugador, incluido el giro de las ruedas
                float v = Input.GetAxis("Vertical");
                float h = Input.GetAxis("Horizontal") * MaxPlayerTurnAngle;
                targetSpeedPct = v;
                targetDir = Quaternion.AngleAxis(h * Mathf.Sign(v), Vector3.up) * tr.forward;
                targetDir.y = 0;
                Debug.DrawRay(tr.position + Vector3.up * 2.5f, targetDir * 4, Color.blue);
                frontalWheelsTargetDir = Quaternion.AngleAxis(h, Vector3.up) * tr.forward;
            }
        } else {
            float d = Vector3.Distance(tr.position, targetNodeTr.position);
            if (d < 2f) {
                TargetNode = TargetNode.GetRandomNextNode();
                targetNodeTr = TargetNode.transform;
            }
            targetDir = (targetNodeTr.position - tr.position).normalized;
            targetDir.y = 0;
        }
        //
        actualSpeedPct = Mathf.Lerp(actualSpeedPct, targetSpeedPct, AccelerationRoughness);
        actualDir = Vector3.Lerp(actualDir, targetDir, TurnRoughness * 5f);
        frontalWheelsActualDir = Vector3.Lerp(frontalWheelsActualDir, frontalWheelsTargetDir, TurnRoughness);
        //
        // para que todas las ruedas giren sobre su eje X segun la velocidad del vehiculo
        // y para que las dos ruedas frontales giren sobre el eje Y simulando la direccion el volante
        float angularSpeed = actualSpeedPct * actualMaxSpeed / 0.55f * Mathf.Rad2Deg * Time.deltaTime;
        for (int i = 0; i < Wheels.Length; i++) {
            Transform wheelTr = Wheels[i];
            wheelTr.rotation *= Quaternion.AngleAxis(angularSpeed, Vector3.right);
            if (i < 2) {
                Transform wheelParentTr = wheelTr.parent;
                Quaternion fromTo = Quaternion.FromToRotation(wheelParentTr.forward, frontalWheelsActualDir) * wheelParentTr.rotation;
                wheelParentTr.rotation = Quaternion.Slerp(wheelParentTr.rotation, fromTo, TurnRoughness * 5);
            }
        }
        //
        DoSound();
    }

    private void FixedUpdate() {
        rb.MovePosition(tr.position + tr.forward * actualSpeedPct * actualMaxSpeed * Time.fixedDeltaTime);
        Vector3 fwd = tr.forward * Mathf.Sign(targetSpeedPct);
        Vector3 dir = actualDir * Mathf.Sign(targetSpeedPct);
        Quaternion fromTo = Quaternion.FromToRotation(fwd, dir) * tr.rotation;
        Quaternion q = Quaternion.Slerp(tr.rotation, fromTo, TurnRoughness * (isPlayerIn ? Mathf.Abs(actualSpeedPct) : 1) * 0.5f);
        //
        // para hacer que los vehículos giren solo sobre el eje Y,
        // y evitar asi que acaben boca abajo o haciendo el pino
        q = Quaternion.Euler(0, q.eulerAngles.y, 0);
        rb.MoveRotation(q);
        Debug.DrawRay(tr.position + Vector3.up * 2.5f, actualDir * 4, Color.cyan);
    }


    // para ponerle el nodo al que debe dirigirse
    public void SetNextNode(Node n) {
        TargetNode = n;
        targetNodeTr = TargetNode.transform;
    }



    // metodo llamado desde ElementsChecker para gestionar 
    // el elemento que tenga el vehiculo delante
    public void ManageColliderEntered(Collider other) {
        if (other.CompareTag("FrontalCar"))
            return;

        // si es un paso de cebra comprueba si esta verde,
        // sino espera a que se ponga verda
        Crosswalk cross = other.GetComponent<Crosswalk>();
        if (cross && cross.CompareTag("CrosswalkVehicle")) {
            if (cross.IsTLGreen) {
                targetSpeedPct = 1;
            } else {
                targetSpeedPct = 0;
                isWaiting = true;
                StartCoroutine(WaitForGreen(cross));
            }
            return;
        }
        // si no conduce el jugador, parara si tiene delante un peaton o al jugador
        if (!isPlayerIn && (other.CompareTag("Passerby") || other.CompareTag("Player"))) {
            targetSpeedPct = 0;
            isWaiting = true;
            StartCoroutine(ClaxonSoundEffect());
            return;
        }
        // si tiene delante un peaton o un zombie lo añade a una lista 
        // que es la que se comprueba para hacerlos explotar
        ///if (isPlayerIn && (other.CompareTag("Passerby") || other.CompareTag("Zombie"))) {
        if (other.CompareTag("Passerby") || other.CompareTag("Zombie")) {
            inFrontZombies.Add(other.GetComponent<Actor>());
            return;
        }
        // si es otro vehiculo:
        VehicleController car = other.GetComponent<VehicleController>();
        if (!car)
            car = other.GetComponentInParent<VehicleController>();
        if (car) {
            if (!IsWaiting()) {  // "¿estoy esperando a alguien?"
                if (IsInFront(car)) {  // "si; ¿el otro vehiculo esta delante de mi?"
                    if (car.WaitingForVehicle != this)  // "si; ¿el otro vehiculo no me esta esperando?"
                        WaitForVehicle(car);  // "si; pues le espero yo a el
                }
            }
        } else {
            targetSpeedPct = 0;
        }
    }


    // metodo llamado desde ElementsChecker para gestionar 
    // el elemento que ya no esta delante del vehiculo
    public void ManageColliderExited(Collider other) {
        VehicleController car = other.GetComponent<VehicleController>();
        if (!car)
            car = other.GetComponentInParent<VehicleController>();
        if (car) {
            if (WaitingForVehicle == car) {
                // si era el vehiculo por el que este vehiculo estaba esperando,
                // entonces deja de esperar y reemprende su camino
                WaitForVehicle(null);
            }
            return;
        }
        // si el peaton (o el jugador) que estaba delante ya no esta sigue su camino
        if (!isPlayerIn && (other.CompareTag("Passerby") || other.CompareTag("Player"))) {
            targetSpeedPct = 1;
            isWaiting = false;
            return;
        }
    }


    private VehicleController WaitingForVehicle;

    // metodo para esperar o dejar de esperar
    private void WaitForVehicle(VehicleController v) {
        if (v == null) {
            isWaiting = false;
            WaitingForVehicle = null;
            targetSpeedPct = 1;
            StartCoroutine(ClaxonSoundEffect());
        } else {
            isWaiting = true;
            WaitingForVehicle = v;
            targetSpeedPct = 0;
        }
    }

    public bool IsWaiting() {
        return isWaiting;
    }

    public bool IsInFront(VehicleController v) {
        Vector3 localVPos = tr.InverseTransformPoint(v.transform.position);
        return localVPos.z > 0;
    }



    // habilitar el control manual del vehiculo
    public void PlayerIn(PECController pl) {
        enabled = true;
        player = pl;
        isPlayerIn = true;
        actualMaxSpeed = MaxPlayerSpeed;
    }

    // deshabilitar el control manual y el vehiculo
    public void PlayerOut() {
        rb.isKinematic = true;
        enabled = false;
        player.GetOutVehicle(this);
    }



    // sonidos del vehiculo para cuando esta parado y en marcha,
    // modificando el pitch segun la velocidad del coche
    private void DoSound() {
        if (actualSpeedPct == 0 && !EngineIdleSound.isPlaying) {
            EngineRunSound.Stop();
            EngineIdleSound.Play();
        } else if (actualSpeedPct > 0) {
            if (!EngineRunSound.isPlaying) {
                EngineIdleSound.Stop();
                EngineRunSound.Play();
            }
            EngineRunSound.pitch = Mathf.Lerp(0.8f, isPlayerIn ? 1.5f : 1.2f, actualSpeedPct);
        }
    }

    // corutina de sonido de claxon cada cierto tiempo aleatorio
    private bool alreadyClaxoning;
    private IEnumerator ClaxonSoundEffect() {
        if (alreadyClaxoning)
            yield break;
        alreadyClaxoning = true;
        while (actualSpeedPct == 0) {
            yield return new WaitForSeconds(Random.Range(0.5f, 3f));
            ClaxonSound.Play();
        }
        alreadyClaxoning = false;
    }



    private IEnumerator WaitForGreen(Crosswalk cross) {
        yield return new WaitUntil(() => cross.IsTLGreen);
        targetSpeedPct = 1;
        isWaiting = false;
    }



    // si choca contra un peaton o un zombie y estan en la lista (parte 
    // frontal del vehiculo) entonces explota.
    // si es un objeto solido solo ejecuta el sonido
    private void OnCollisionEnter(Collision collision) {
        string tag = collision.collider.tag;
        string name = collision.collider.name;
        if (tag.Equals("Passerby") || tag.Equals("Zombie")) {
            if (actualSpeedPct > 0.1f) {
                Actor actor = collision.collider.GetComponent<Actor>();
                if (inFrontZombies.Contains(actor)) {
                    if (isPlayerIn)
                        GameManager.UpdateZombiesKilled(actor is ZombieController);
                    actor.Explode();
                    inFrontZombies.Remove(actor);
                }
            }
        } else if (collision.collider.GetComponent<PECController>() == null) {
            ContactPoint[] cp = new ContactPoint[collision.contactCount];
            collision.GetContacts(cp);
            // para evitar que el sonido se reproduzca a cada frame,
            // ya que las ruedas estan en constante contacto con el suelo,
            // ignora dicho contacto y solo tiene en cuenta el contacto del
            // propio vehiculo con un objeto solido
            foreach (ContactPoint c in cp)
                if (c.thisCollider.name.Contains("Wheel"))
                    return;
            if (CollisionSound) {
                CollisionSound.volume = actualSpeedPct;
                CollisionSound.Play();
            }
        }
    }

}

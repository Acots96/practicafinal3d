﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWeaponController : Weapon {

    [SerializeField] private float Amount, BulletsPerCartridge;

    [SerializeField] private ParticleSystem FireEffect, BulletImpactEffect, EnemyImpactEffect;
    [SerializeField] private AudioSource FireSound, ReadySound, ReloadSound;
    [SerializeField] private AudioClip HitSolidSound, HitBodySound;

    private Transform tr, parentTr;
    public float LoadedBullets { get; private set; }
    public float CartridgesBullets { get; private set; }


    private void Awake() {
        tr = transform;
        parentTr = GetComponentInParent<PECController>().transform;

        CartridgesBullets = Amount - BulletsPerCartridge;
        LoadedBullets = BulletsPerCartridge;
    }
    


    public bool CanShoot() {
        return LoadedBullets > 0;
    }

    public override void Attack(Vector3 origin, Vector3 direction) {
        Debug.Log("SHOOT");
        if (FireEffect)
            FireEffect.Play();
        Fire(origin, direction);
    }

    private void Fire(Vector3 origin, Vector3 direction) {
        if (LoadedBullets == 0)
            return;
        if (FireSound) {
            FireSound.Play();
        }
        Amount = --LoadedBullets + CartridgesBullets;
        Ray ray = new Ray(origin, direction);
        Debug.DrawRay(ray.origin, ray.direction * 200f, Color.cyan);
        // raycast
        if (Physics.Raycast(ray, out RaycastHit hit, 200f)) {
            Debug.Log(hit.collider);
            Actor actor = hit.transform.GetComponent<Actor>();
            ParticleSystem effect;
            if (actor) {
                effect = EnemyImpactEffect;
                if (actor is ZombieController) {
                    float dmg = Damage * Random.Range(0.8f, 1.1f);
                    if (hit.collider.gameObject.CompareTag("Head"))
                        dmg *= 2.5f;
                    actor.TakeDamage(dmg, gameObject);
                } else if (actor is PasserbyController) {
                    GameManager.UpdateZombiesKilled(false);
                    actor.Explode();
                }
            } else {
                effect = BulletImpactEffect;
            }
            // efectos de impacto
            Quaternion rot = Quaternion.Euler(hit.normal) * Quaternion.Euler(-90, 0, 0);
            GameObject impact = Instantiate(effect.gameObject, hit.point, rot);
            impact.GetComponent<ParticleSystem>().Play();
            if (Type == GameManager.WeaponType.Shotgun) {
                GameObject impact2 = Instantiate(effect.gameObject, hit.point, rot);
                impact2.GetComponent<ParticleSystem>().Play();
                impact.transform.localScale *= 1.5f;
                impact2.transform.localScale *= 1.5f;
            }
            // sonido de impacto
            AudioSource aS = impact.AddComponent<AudioSource>();
            aS.clip = actor != null ? HitBodySound : HitSolidSound;
            aS.pitch = Type == GameManager.WeaponType.Pistol ? Random.Range(1f, 1.1f) : Random.Range(0.9f, 1f);
            aS.volume = Type == GameManager.WeaponType.Pistol ? 0.6f : 1f;
            aS.maxDistance = Type == GameManager.WeaponType.Pistol ? 15 : 30;
            aS.Play();
        }
    }


    public bool CanReload() {
        return Amount > 0 && LoadedBullets < BulletsPerCartridge;
    }

    public void Reload() {
        float askedBullets = BulletsPerCartridge - LoadedBullets;
        float actualCartridgesBullets = Mathf.Max(0, CartridgesBullets - askedBullets);
        LoadedBullets += CartridgesBullets - actualCartridgesBullets;
        CartridgesBullets = actualCartridgesBullets;
        PlayReadySound();
    }

    public void PlayReloadSound() {
        if (ReloadSound)
            ReloadSound.Play();
    }


    public void AddAmmo(int amount) {
        Amount += amount;
        CartridgesBullets = Amount - LoadedBullets;
    }


    public void PlayReadySound() {
        if (ReadySound)
            ReadySound.Play();
    }

}

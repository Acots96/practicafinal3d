﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasserbyController : Actor, ElementsChecker.IElementsChecker {

    private enum State { Wander, Run, Dead }

    [SerializeField] private float RunSpeed, WalkSpeedPct, MinTimeRunning = 5;
    [SerializeField, Range(0.01f, 1f)] private float TurnRoughness, AccelerationRoughness;
    [SerializeField] private float ZombiesDetectionRange, DetectingTimeFrequency;
    [SerializeField] private LayerMask ZombiesMask;

    [SerializeField] private AudioSource[] ScreamSound;

    private Transform tr, targetNodeTr;
    private Rigidbody rb;
    private CapsuleCollider col;
    private Animator anim;
    public Node targetNode, previousNode;
    private PassersbySpawner spawner;

    private float speed, targetSpeed;
    private Vector3 targetDir;
    private float detectingTimeFreq;
    private bool zombiesSeen, isRunning, keepRunning;

    private State state;
    private Transform attackerTr;

    private ElementsChecker checker;


    private void Awake() {
        tr = transform;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        detectingTimeFreq = 0;
        targetSpeed = RunSpeed * WalkSpeedPct;
        checker = GetComponentInChildren<ElementsChecker>();
    }

    private void Start() {
        spawner = GetComponentInParent<PassersbySpawner>();
    }


    private void Update() {
        if (state == State.Dead)
            return;

        detectingTimeFreq += Time.deltaTime;
        if (detectingTimeFreq >= DetectingTimeFrequency) {
            detectingTimeFreq = 0;
            CheckZombies();
        }
        //
        if (!zombiesSeen && targetNode) {
            float d = Vector3.Distance(tr.position, targetNodeTr.position);
            if (d < 1) {
                previousNode = targetNode;
                targetNode = targetNode.GetRandomNextNodeExcept(targetNode, previousNode);
                targetNodeTr = targetNode.transform;
                targetDir = (targetNodeTr.position - tr.position).normalized;
                targetDir.y = 0;
            }
        }
        //
        speed = Mathf.Lerp(speed, targetSpeed, AccelerationRoughness);
        anim.SetFloat("Speed", speed / RunSpeed);
    }

    private void FixedUpdate() {
        rb.MovePosition(tr.position + tr.forward * targetSpeed * Time.fixedDeltaTime);
        Quaternion q = Quaternion.AngleAxis(Vector3.Angle(tr.forward, targetDir), Vector3.up);
        q = Quaternion.Euler(0, q.eulerAngles.y, 0);
        rb.MoveRotation(Quaternion.Slerp(tr.rotation, q * tr.rotation, TurnRoughness));
    }


    public void SetNextNode(Node n) {
        targetNode = n;
        targetNodeTr = n.transform;
    }


    public override void TakeDamage(float dmg, GameObject atckr) {
        if (state == State.Dead)
            return;
        state = State.Dead;
        attackerTr = atckr.transform;
        speed = targetSpeed = 0;
        anim.SetTrigger("DoDie");
        rb.isKinematic = true;
        col.enabled = false;
        Invoke("BecomeZombie", 2f);
        spawner.InstantiateRandomPasserby();
    }

    private void BecomeZombie() {
        GameManager.BecomeZombie(this, attackerTr);
        Destroy(gameObject);
    }



    // comprueba si hay zombies en un radio determinado
    private void CheckZombies() {
        Collider[] cols = Physics.OverlapSphere(tr.position, ZombiesDetectionRange, ZombiesMask.value);
        List<Transform> zombies = new List<Transform>();
        foreach (Collider c in cols) {
            if (c.GetComponent<ZombieController>())
                zombies.Add(c.transform);
        }
        // en caso de qe fuera perseguido pero ya no, sigue su wander
        if (zombiesSeen && zombies.Count == 0) {  //was chased, but not anymore
            if (spawner) {
                targetNode = spawner.FindClosestPasserbyNode(tr.position);
                Debug.Log(targetNode);
                targetNodeTr = targetNode.transform;
                targetDir = (targetNodeTr.position - tr.position).normalized;
                targetDir.y = 0;
            }
            isRunning = false;
            targetSpeed = RunSpeed * WalkSpeedPct;
            //
            if (checker)
                checker.enabled = true;
        } else if (!zombiesSeen && zombies.Count > 0) {  // ha visto zombies, empieza a correr
            if (ScreamSound != null && ScreamSound.Length > 0) {
                ScreamSound[Random.Range(0, ScreamSound.Length)].Play();
            }
        }
        zombiesSeen = zombies.Count > 0;
        anim.SetBool("ZombiesSeen", zombiesSeen);
        //
        // calculo de la direccion de huida
        if (zombiesSeen) {
            targetNode = null;
            targetNodeTr = null;
            Vector3 pos = tr.position;
            Vector3 dir = Vector3.zero;
            foreach (Transform z in zombies) {
                dir += (pos - z.position).normalized;
            }
            targetDir = dir.normalized;
            if (!isRunning) {
                targetDir = -targetDir;
            } else {
                // check if wall (?)
            }
            targetDir.y = 0;
            //
            if (checker)
                checker.enabled = false;
        }
    }


    // metodo llamado desde PasserbyRunAnimationBehaviour para 
    // indicar que ya puede empezar a correr
    public void StartAnimationRun() {
        isRunning = true;
        targetSpeed = RunSpeed;
        targetDir = -targetDir;
        targetDir.y = 0;
        tr.forward = targetDir;
    }


    // pitch distintos segun el paso que sea, para dar la sensacion
    // de alternar entre paso derecho e izquierdo
    public override void PlayStepSound(int i) {
        AudioSource step = StepSounds[0];
        if (i == 0)
            step.pitch = Random.Range(0.7f, 1.1f);
        else
            step.pitch = Random.Range(0.9f, 1.3f);
        step.Play();
    }



    // si se encuentra con un cruce hace lo mismo que el vehiculo,
    // pero si se encuentra con un vehiculo simplemente espera
    public void ManageColliderEntered(Collider collider) {
        if (zombiesSeen)
            return;
        //
        Crosswalk cross = collider.GetComponent<Crosswalk>();
        if (cross && cross.CompareTag("CrosswalkPasserby")) {
            if (cross.IsTLGreen) {
                targetSpeed = RunSpeed * WalkSpeedPct;
            } else {
                targetSpeed = 0;
                StartCoroutine(WaitForGreen(cross));
            }
            return;
        }
        //
        VehicleController v = collider.GetComponent<VehicleController>();
        if (v)
            targetSpeed = 0;
    }

    // cuando el vehiculo se va entonces sigue su camino
    public void ManageColliderExited(Collider collider) {
        if (zombiesSeen)
            return;
        //
        VehicleController v = collider.GetComponent<VehicleController>();
        if (v)
            targetSpeed = RunSpeed * WalkSpeedPct;
    }



    private IEnumerator WaitForGreen(Crosswalk cross) {
        yield return new WaitUntil(() => cross.IsTLGreen);
        targetSpeed = RunSpeed * WalkSpeedPct;
    }



    // metodo de la explosion.
    // instancia el efecto de particulas de sangre junto a las partes del cuerpo.
    public override void Explode() {
        GameObject explosion = Instantiate(ExplosionEffectPrefab, tr.position, Quaternion.identity);
        AudioSource aS = explosion.GetComponent<AudioSource>();
        aS.pitch = Random.Range(0.85f, 1.15f);
        aS.Play();
        //
        int parts = (int) (Random.Range(0.15f, 0.4f) * ExplosionBodyPartsPrefabs.Count);
        Vector3 center = tr.TransformPoint(col.center);
        for (int i = 0; i < parts; i++) {
            Vector3 pos = center + Vector3.up * Random.Range(-1f, 1f);
            GameObject prefab = ExplosionBodyPartsPrefabs[Random.Range(0, ExplosionBodyPartsPrefabs.Count)];
            ExplosionBodyPartsPrefabs.Remove(prefab);
            GameObject part = Instantiate(prefab, pos, Quaternion.identity);
            //
            Vector3 force = new Vector3(Random.Range(-1f, 1f), Random.value, Random.Range(-1f, 1f)) * 5f;
            part.GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);
            //
            Destroy(part, 6f);
        }
        //
        spawner.InstantiateRandomPasserby();
        Destroy(gameObject);
    }

}

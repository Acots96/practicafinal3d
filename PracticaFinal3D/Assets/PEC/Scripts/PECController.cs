﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PECController : Actor {

    [SerializeField] private float WalkSpeedPct, RunSpeed;
    [SerializeField] private float Acceleration, MinFov, MaxFov;
    [SerializeField] private CinemachineBrain BrainCam;
    [SerializeField] private CinemachineFreeLook VCam, VCamAim;
    [SerializeField] private CinemachineVirtualCamera VCamVehicle;

    [SerializeField] private Weapon[] Weps;
    [SerializeField] private GameObject[] FakeWeps;

    [SerializeField] private Transform UpBodyTr;

    [SerializeField] private ParticleSystem ReceiveImpactEffect;

    [SerializeField] private AudioSource JumpSound, LandSound;
    [SerializeField] private AudioSource ChangeWepSound;


    public bool IsDead { get; private set; }
    public bool CanMove;

    public SwordController Sword { get; private set; }

    private Transform tr;
    private Animator anim;
    private Rigidbody rb;
    private CapsuleCollider bodyCol;
    private Transform cameraTr;

    private float speedZ, speedX, walkSpeed, speedWhenTurn;
    private Vector3 actualDir, targetDir;
    private bool aimingAndCombatMove, wasAiming, moveForward, moveBackward, moveLeft, 
        moveRight, running, jumping, turning, changingWep, reloading, shooting, lockVCam;
    private int actualWeaponIdx, nextWeaponIdx;
    public int keys;

    private VehicleController actualVehicle;
    private bool isDriving;
    [SerializeField] private GameObject[] RendererGO;


    private void Awake() {
        tr = transform;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        bodyCol = GetComponent<CapsuleCollider>();
        cameraTr = Camera.main.transform;

        walkSpeed = RunSpeed * WalkSpeedPct;
        speedZ = speedX = 0;
        anim.SetFloat("Speed", speedZ / RunSpeed);
        actualWeaponIdx = keys = 0;
        targetDir = tr.forward;
        actualDir = targetDir;

        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
    }

    /**
     * Actualizacion de info de pantalla
     */
    private void Start() {
        GameManager.UpdateWeaponImage(Weps[actualWeaponIdx].Type);
        FireWeaponController fw = Weps[actualWeaponIdx] as FireWeaponController;
        if (fw)
            GameManager.UpdateWeaponText(fw.LoadedBullets, fw.CartridgesBullets);
        GameManager.UpdateLife(Life);
        GameManager.UpdateKeys(keys);
        for (int i = 1; i < Weps.Length; i++) {
            Weps[i].gameObject.SetActive(false);
            FakeWeps[i].SetActive(false);
        }
        Sword = Weps[Weps.Length - 1] as SwordController;
    }


    private void Update() {
        // pulsar Escape para liberar el mouse
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            VCam.m_XAxis.m_MaxSpeed = 0;
            VCam.m_YAxis.m_MaxSpeed = 0;
            //Debug.Break();
        }
        // pulsar de nuevo en la pantalla para volver al juego
        if (Input.GetMouseButtonDown(1) && Cursor.visible) {
            HideMouse();
            return;
        }
        if (IsDead || !CanMove)
            return;

        CheckInput();
        
        if (turning)
            anim.SetFloat("Speed", speedWhenTurn / RunSpeed);
        else
            anim.SetFloat("Speed", speedZ / RunSpeed);

        if (aimingAndCombatMove) {
            bool moveX = moveLeft || moveRight;
            bool moveZ = moveForward || moveBackward;
            //anim.SetBool("IsStrafeForward", moveForward);
            anim.SetFloat("SpeedStrafeLeftRight", moveX ? (moveLeft ? -1 : 1) : 0);
            anim.SetFloat("SpeedStrafeForwardBackward", moveZ ? (moveBackward ? -1 : 1) : 0);
        }
        anim.SetBool("IsCombatMove", aimingAndCombatMove);

        if (Input.GetKeyDown(KeyCode.E)) { // interaction
            if (actualNKZ) {
                if (!actualNKZ.IsLock) {
                    actualNKZ.InvokeEvent();
                    GameManager.ShowEmptyInfo();
                }
            } else if (isInEndZone) {
                CanMove = false;
                GameManager.Win();
            } else if (actualVehicle) {
                isDriving = !isDriving;
                GetInVehicle(actualVehicle);
            }
        }
    }

    public void HideMouse() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        VCam.m_XAxis.m_MaxSpeed = 300;
        VCam.m_YAxis.m_MaxSpeed = 5;
    }


    private void FixedUpdate() {
        if (IsDead || !CanMove)
            return;

        if (turning)
            rb.MovePosition(rb.position + actualDir * 0 * Time.fixedDeltaTime);
        else
            rb.MovePosition(rb.position + actualDir * speedZ * Time.fixedDeltaTime);
    }


    private void LateUpdate() {
        if (IsDead)
            return;

        // Si esta apuntando tiene que mirar siempre hacia adelante,
        // sino en la direccion hacia la que se dirija.
        if (aimingAndCombatMove) {
            Vector3 dir = VCamAim.transform.forward;
            dir.y = 0;
            tr.forward = Vector3.Lerp(tr.forward, dir, 1f);
        } else {
            actualDir.y = 0;
            tr.forward = actualDir;
        }

        // Ajustes de la direccion del UpBody segun el arma y si esta apuntando o no.
        Vector3 targetForward = UpBodyTr.forward;
        if (!running) {
            switch (Weps[actualWeaponIdx].tag) {
                case "Pistol":
                    targetForward = (tr.forward + tr.right * 2f).normalized;
                    if (aimingAndCombatMove) {
                        targetForward = (tr.forward + tr.right * 4.5f).normalized;
                        targetForward.y += (VCamAim.m_Follow.position - VCamAim.transform.position).normalized.y;
                        targetForward.y += 0.75f;
                        if (moveForward)
                            targetForward.y -= 0.025f;
                        else if (moveBackward)
                            targetForward.y += 0.025f;
                        targetForward.y *= 8f;
                    }
                    UpBodyTr.forward = Vector3.Lerp(UpBodyTr.forward, targetForward, aimingAndCombatMove ? 0.35f : 0.35f);
                    break;
                case "Shotgun":
                    targetForward = (tr.forward + tr.right * 1.25f).normalized;
                    if (aimingAndCombatMove) {
                        targetForward.y += (VCamAim.m_Follow.position - VCamAim.transform.position).normalized.y;
                        targetForward.y += 0.78f;
                        targetForward.y *= 8f;
                    }
                    float pct = aimingAndCombatMove ? 0.35f : 0.35f;
                    if (speedZ > 0)
                        pct *= 1f;
                    UpBodyTr.forward = Vector3.Lerp(UpBodyTr.forward, targetForward, pct);
                    break;
                case "Sword":
                    targetForward = (tr.forward - tr.right * 0.75f - tr.up * 0.25f).normalized;
                    UpBodyTr.forward = Vector3.Lerp(UpBodyTr.forward, targetForward, aimingAndCombatMove ? 0.35f : 0.35f);
                    break;
            }
        }
    }



    private void CheckInput() {
        if (!aimingAndCombatMove && Input.GetAxis("Mouse ScrollWheel") != 0) {
            float fov = VCam.m_Lens.FieldOfView;
            fov = Mathf.Max(MinFov, Mathf.Min(MaxFov, fov - Input.GetAxis("Mouse ScrollWheel") * 10f));
            VCam.m_Lens.FieldOfView = fov;
        }

        moveForward = Input.GetKey(KeyCode.W);
        moveBackward = Input.GetKey(KeyCode.S);
        moveLeft = Input.GetKey(KeyCode.A);
        moveRight = Input.GetKey(KeyCode.D);
        aimingAndCombatMove = Input.GetMouseButton(1);
        running = Input.GetKey(KeyCode.LeftShift) && !aimingAndCombatMove && !changingWep && !reloading;

        float multiplier;
        
        // En el caso de que se mueva y no gire
        if ((moveForward || moveBackward || moveLeft || moveRight) && !turning) {
            // el movimiento depende de la rotacion de la camara
            targetDir = Convert.ToInt32(moveForward) * cameraTr.forward;
            targetDir += Convert.ToInt32(moveBackward) * -cameraTr.forward;
            targetDir += Convert.ToInt32(moveLeft) * -cameraTr.right;
            targetDir += Convert.ToInt32(moveRight) * cameraTr.right;
            targetDir.y = 0;
            // para girar si la direccion nueva tiene una diferencia superior
            // a 120 grados respecto a la anterior (eje vertical).
            if (!aimingAndCombatMove && Vector3.Angle(tr.forward, targetDir) > 120 && !turning) {
                turning = true;
                speedWhenTurn = speedZ;
                anim.applyRootMotion = true;
                anim.SetBool("IsTurn", true);
            }
            // aumenta la velocidad
            multiplier = 1;
        } else {
            // disminuye la velocidad al no moverse
            multiplier = -1;
        }
        targetDir = targetDir.normalized;

        // comprobar si debe apuntar o no
        AimEffect(aimingAndCombatMove);

        // calcula la velocidad dependiendo de si esta girando o no
        if (!turning) {
            actualDir = Vector3.Slerp(actualDir, targetDir, aimingAndCombatMove ? 1f : 0.15f);
            speedZ += Acceleration * Time.deltaTime * multiplier;
            float maxSpeed = running ? RunSpeed : walkSpeed;
            speedZ = Mathf.Max(0, Mathf.Min(maxSpeed, speedZ));
        } else {
            actualDir = Quaternion.Euler(anim.deltaRotation.eulerAngles) * actualDir;
            if (Vector3.Angle(actualDir, targetDir) < 60f) { // finaliza el giro
                turning = anim.applyRootMotion = false;
                anim.SetBool("IsTurn", false);
            }            
        }

        // Change wep
        CheckChangeWeapon();

        // Reload wep
        CheckReloadWeapon();

        // Shooting
        CheckShooting();

        if (!jumping) {
            jumping = Input.GetKeyDown(KeyCode.Space) && !aimingAndCombatMove;
            if (jumping) {
                anim.SetTrigger("DoJump");
            }
        }
    }


    // solo puede equiparse un arma si la tiene,
    // para saber si la tiene se comprueba si la
    // fakewp esta activada
    private void CheckChangeWeapon() {
        int wep = actualWeaponIdx;
        if (Input.GetKeyDown(KeyCode.Alpha1)) // pistola
            wep = 0;
        else if (Input.GetKeyDown(KeyCode.Alpha2)) // escopeta
            wep = 1;
        else if (Input.GetKeyDown(KeyCode.Alpha3)) // espada
            wep = 2;
        //
        if (wep != actualWeaponIdx && FakeWeps[wep].activeSelf && !changingWep && !reloading && !shooting && !running) {
            changingWep = true;
            nextWeaponIdx = wep;
            Weapon nextWep = Weps[wep];
            anim.SetBool("IsPistol", nextWep.CompareTag("Pistol"));
            anim.SetBool("IsShotgun", nextWep.CompareTag("Shotgun"));
            anim.SetBool("IsSword", nextWep.CompareTag("Sword"));
            GameManager.UpdateWeaponImage(nextWep.Type);
            // sonido de cambiar del arma actual
            if (Weps[actualWeaponIdx] is FireWeaponController) {
                ChangeWepSound.pitch = UnityEngine.Random.Range(0.85f, 1.15f);
                ChangeWepSound.Play();
            } else {
                (Weps[actualWeaponIdx] as SwordController).PlaySheatheSound();
            }
            // actualizar info del arma siguiente
            FireWeaponController fw = nextWep as FireWeaponController;
            if (fw) {
                GameManager.UpdateWeaponText(fw.LoadedBullets, fw.CartridgesBullets);
            } else {
                GameManager.UpdateWeaponText(-1, -1);
            }
            if (aimingAndCombatMove)
                GameManager.UpdateWeaponTarget(nextWep.Type);
        }
    }

    // llamado desde ChangeWeaponAnimationBehaviour para 
    // activar/desactivar el arma correspondiente y 
    // activar/desactivar el arma fake
    public void ChangeWeaponEffect(bool enable) {
        if (!enable) {
            Weps[actualWeaponIdx].gameObject.SetActive(false);
            FakeWeps[actualWeaponIdx].SetActive(true);
            actualWeaponIdx = nextWeaponIdx;
        }
        if (enable) {
            FakeWeps[actualWeaponIdx].SetActive(false);
            Weps[actualWeaponIdx].gameObject.SetActive(true);
            if (Weps[actualWeaponIdx] is SwordController) {
                (Weps[actualWeaponIdx] as SwordController).PlaySheatheSound();
            } else {
                ChangeWepSound.pitch = UnityEngine.Random.Range(0.85f, 1.15f);
                ChangeWepSound.Play();
                //Invoke("StopChangeWepSound", 0.5f);
            }
        }
    }

    // arma sacada completamente
    public void FinishDrawWeapon() {
        changingWep = false;
        ChangeWepSound.Stop();
        if (Weps[actualWeaponIdx] is FireWeaponController)
            (Weps[actualWeaponIdx] as FireWeaponController).PlayReadySound();
    }



    private void CheckReloadWeapon() {
        if (Input.GetKeyDown(KeyCode.R) && !changingWep && !reloading && !shooting && !running) {
            FireWeaponController fw = Weps[actualWeaponIdx] as FireWeaponController;
            if (fw && fw.CanReload()) {
                fw.PlayReloadSound();
                reloading = true;
                anim.SetTrigger("DoReload");
                Invoke("ReloadInvoke", 2f);
            }
        }
    }

    private void ReloadInvoke() {
        Reload();
        reloading = false;
    }

    private void Reload() {
        FireWeaponController fw = Weps[actualWeaponIdx] as FireWeaponController;
        fw.Reload();
        GameManager.UpdateWeaponText(fw.LoadedBullets, fw.CartridgesBullets);
    }



    private void CheckShooting() {
        if (!Cursor.visible && Input.GetMouseButtonDown(0) && !shooting && !changingWep && !reloading && !running) {
            FireWeaponController fw = Weps[actualWeaponIdx] as FireWeaponController;
            if (fw) {
                if (fw.CanShoot())
                    shooting = true;
                else
                    return;
            }
            //shooting = Weps[actualWeaponIdx] is FireWeaponController;
            anim.SetTrigger("DoShoot");
        }
    }

    // llamado desde ShootAnimationBehaviour para ejecutar el disparo 
    // o el golpe de espada.
    // en caso de estar apuntando calcula la direccion
    public void Shoot() {
        if (Weps[actualWeaponIdx] is SwordController)
            anim.SetFloat("SwordAttackNum", UnityEngine.Random.Range(0, 2));
        //
        Vector3 origin = Weps[actualWeaponIdx].transform.position;
        Vector3 direction = tr.forward;
        //
        if (aimingAndCombatMove) {
            Vector3 localOffset = VCamAim.GetComponent<CinemachineCameraOffset>().m_Offset;
            Transform camTr = VCamAim.transform;
            Vector3 worldOffset = camTr.right * localOffset.x + camTr.up * localOffset.y + camTr.forward * localOffset.z;
            origin = VCamAim.transform.position + worldOffset;
            direction = VCamAim.transform.forward;
        }
        //
        Weps[actualWeaponIdx].Attack(origin, direction);
        FireWeaponController fw = Weps[actualWeaponIdx] as FireWeaponController;
        if (fw)
            GameManager.UpdateWeaponText(fw.LoadedBullets, fw.CartridgesBullets);
    }

    // llamado tambien desde ShootAnimationBehaviour
    public void FinishShoot() {
        shooting = false;
    }




    public void JumpForce() {
        if (JumpSound) {
            JumpSound.pitch = UnityEngine.Random.Range(0.7f, 1.3f);
            JumpSound.Play();
            bodyCol.height = 1.5f;
            Invoke("PlayLandSound", 0.5f);
        }
        rb.AddForce(tr.up * 4f, ForceMode.Impulse);
    }
    private void PlayLandSound() {
        if (LandSound) {
            LandSound.pitch = UnityEngine.Random.Range(0.7f, 1.3f);
            LandSound.Play();
        }
    }
    public void FinishJump() {
        jumping = false;
        bodyCol.height = 1.8f;
        anim.ResetTrigger("DoJump");
    }


    // activa/desactiva la VCam que toque
    private void AimEffect(bool aim) {
        if (aim == wasAiming)
            return;
        wasAiming = aim;
        if (aim) {
            GameManager.UpdateWeaponTarget(Weps[actualWeaponIdx].Type);
            targetDir = VCam.transform.forward;
        } else {
            GameManager.UpdateWeaponTarget(GameManager.WeaponType.None);
        }
        VCam.gameObject.SetActive(!aim);
        VCamAim.gameObject.SetActive(aim);
    }

    // activa/desactiva la camara del vehiculo
    private void VehicleVCam(bool enable, Transform vehicleTr) {
        VCam.gameObject.SetActive(!enable);
        VCamAim.gameObject.SetActive(false);
        VCamVehicle.gameObject.SetActive(enable);
        if (enable) {
            VCamVehicle.LookAt = vehicleTr;
            VCamVehicle.Follow = vehicleTr;
        }
    }




    public override void TakeDamage(float dmg, GameObject atckr) {
        if (IsDead)
            return;
        Life = (int) Mathf.Max(0, Life - dmg);
        GameManager.UpdateLife(Life);
        GameObject impact = Instantiate(ReceiveImpactEffect.gameObject, UpBodyTr.position, Quaternion.identity);
        impact.GetComponent<ParticleSystem>().Play();
        if (Life <= 0) {
            IsDead = true;
            turning = shooting = reloading = changingWep = false;
            rb.freezeRotation = true;
            rb.useGravity = false;
            GetComponent<CapsuleCollider>().enabled = false;
            anim.SetTrigger("DoDie");
            AimEffect(false);
            if (DieSound)
                DieSound.Play();
            GameManager.GameOver();
        } else {
            if (HitSounds != null && HitSounds.Length > 0)
                HitSounds[UnityEngine.Random.Range(0, HitSounds.Length)].Play();
            anim.SetTrigger("DoGetHit");
        }
    }


    
    private int stepSoundIdx;

    // pitch distintos segun el paso que sea, para dar la sensacion
    // de alternar entre paso derecho e izquierdo
    public override void PlayStepSound(int i) {
        AudioSource step = StepSounds[stepSoundIdx++ % StepSounds.Length];
        if (i == 0)
            step.pitch = UnityEngine.Random.Range(0.7f, 1.1f);
        else
            step.pitch = UnityEngine.Random.Range(0.9f, 1.3f);
        step.Play();
    }



    private void OnCollisionEnter(Collision collision) {
        Item item = collision.collider.GetComponent<Item>();
        if (item) {
            AddItem(item.Type, item.Amount);
            item.Grab();
        }
    }

    private void AddItem(Item.ItemType type, int amount) {
        switch (type) {
            case Item.ItemType.AmmoPistol:
                foreach (Weapon w in Weps) {
                    if (w.Type == GameManager.WeaponType.Pistol) {
                        FireWeaponController fw = w as FireWeaponController;
                        fw.AddAmmo(amount);
                        if (Weps[actualWeaponIdx].Equals(fw))
                            GameManager.UpdateWeaponText(fw.LoadedBullets, fw.CartridgesBullets);
                        break;
                    }
                }
                break;
            case Item.ItemType.AmmoShotgun:
                foreach (Weapon w in Weps) {
                    if (w.Type == GameManager.WeaponType.Shotgun) {
                        FireWeaponController fw = w as FireWeaponController;
                        fw.AddAmmo(amount);
                        if (Weps[actualWeaponIdx].Equals(fw))
                            GameManager.UpdateWeaponText(fw.LoadedBullets, fw.CartridgesBullets);
                        break;
                    }
                }
                break;
            case Item.ItemType.Life:
                Life = Mathf.Min(100, Life + amount);
                GameManager.UpdateLife(Life);
                break;
            case Item.ItemType.Key:
                keys += amount;
                GameManager.UpdateKeys(keys);
                GameManager.ShowKeyFoundInfo();
                break;
            case Item.ItemType.WeaponShotgun:
                FakeWeps[1].SetActive(true);
                HideMouse();
                GameManager.ShowNewWeaponInfo(type);
                break;
            case Item.ItemType.WeaponSword:
                FakeWeps[2].SetActive(true);
                HideMouse();
                GameManager.ShowNewWeaponInfo(type);
                break;
        }
    }



    // metodo ejecutado cuando el personaje sube a un vehiculo.
    // lo "desactiva" y avisa al VehicleController para que 
    // ahora seas el receptor del input
    private void GetInVehicle(VehicleController vehicle) {
        Input.ResetInputAxes();
        foreach (GameObject go in RendererGO)
            go.SetActive(false);
        tr.position = vehicle.transform.position;
        tr.parent = vehicle.transform;
        rb.isKinematic = true;
        bodyCol.enabled = false;
        vehicle.PlayerIn(this);
        VehicleVCam(true, vehicle.transform);
        enabled = false;
    }

    // metodo ejecutado cuando el jugador decide salir del vehiculo,
    // lamado desde VehicleController, para "activar" al jugador de nuevo.
    public void GetOutVehicle(VehicleController vehicle) {
        Input.ResetInputAxes();
        tr.parent = null;
        tr.position = vehicle.transform.TransformPoint(Vector3.left * 1.5f);
        foreach (GameObject go in RendererGO)
            go.SetActive(true);
        rb.isKinematic = false;
        bodyCol.enabled = true;
        VehicleVCam(false, vehicle.transform);
        actualVehicle = null;
        enabled = true;
    }




    private NeedKeyZone actualNKZ;
    private bool isInEndZone;

    private void OnTriggerEnter(Collider other) {
        NeedKeyZone nkz = other.GetComponent<NeedKeyZone>();
        // si es un NeedKeyZone comprueba si el jugador la tiene,
        // si es asi entonces desbloquea la zona,
        // sino informa al jugador
        if (nkz) {
            actualNKZ = nkz;
            if (nkz.IsLock) {
                if (keys >= actualNKZ.Amount) {
                    actualNKZ.LockIt(false);
                    keys -= actualNKZ.Amount;
                    GameManager.UpdateKeys(keys);
                    GameManager.ShowPressEInfo();
                } else {
                    GameManager.ShowLockInfo();
                }
            } else {
                GameManager.ShowPressEInfo();
            }
            // si es la endzone muestra el mensaje por pantalla una 
            // sola vez e informa al jugador sobre lo que debe hacer
        } 
        else if (other.CompareTag("EndZone")) {
            isInEndZone = true;
            GameManager.EndZone(true);
        } 
        else if (other.CompareTag("Vehicle")) {
            GameManager.ShowPressEInfo();
            actualVehicle = other.GetComponent<VehicleController>();
        }
    }

    private void OnTriggerExit(Collider other) {
        NeedKeyZone nkz = other.GetComponent<NeedKeyZone>();
        if (nkz) {
            actualNKZ = null;
            GameManager.ShowEmptyInfo();
        } 
        else if (other.CompareTag("EndZone")) {
            isInEndZone = false;
            GameManager.EndZone(false);
        } 
        else if (other.CompareTag("Vehicle")) {
            GameManager.ShowEmptyInfo();
            actualVehicle = null;
        }
    }

}

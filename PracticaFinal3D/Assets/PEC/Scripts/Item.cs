﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public enum ItemType { AmmoPistol, AmmoShotgun, Life, Key, WeaponShotgun, WeaponSword }

    public ItemType Type;
    public int Amount;

    [SerializeField]
    private AudioSource GrabSound;


    private void Awake() {
        if (Type == ItemType.AmmoPistol || Type == ItemType.AmmoShotgun)
            Amount = (int)(Amount * Random.Range(0.7f, 1f));
    }


    public void Grab() {
        if (GrabSound)
            GrabSound.Play();
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().useGravity = false;
        Destroy(gameObject, GrabSound.clip.length);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiesSpawner : MonoBehaviour {

    [SerializeField] private bool AllowZombiesCreation;
    [SerializeField] private List<GameObject> ZombiesPrefabs;
    [SerializeField] private GameObject ZombieStrongPrefab, KeyPrefab;
    [SerializeField] private int ZombieStrongStartAmount;
    [SerializeField] private float StartSpawnTime, MinSpawnTime, DecreaseTime;
    [SerializeField] private int LimitZombiesPerPoint;

    private List<Transform> spawnPoints;


    /**
     * - Spawnear una cantidad inicial aleatoria de zombies en cada punto.
     * - Spawnear una cantidad concreta de zombies fuertes, uno de los cuales lleva una llave.
     */
    private void Start() {
        if (!AllowZombiesCreation)
            return;
        spawnPoints = new List<Transform>();
        foreach (Transform p in transform) {
            spawnPoints.Add(p);
            //start zombies
            int zombies = Random.Range(2, 4);
            for (int i = 0; i < zombies; i++) {
                GameObject zombie = ZombiesPrefabs[Random.Range(0, ZombiesPrefabs.Count)];
                GameObject instance = Instantiate(zombie, p.position, Quaternion.identity);
                instance.transform.SetParent(p);
                GameManager.AddActor(instance);
            }
        }
        //
        StartCoroutine(SpawnZombiesEffect());
        //
        List<Transform> zombieStrongSpawnPoints = new List<Transform>(spawnPoints);
        List<ZombieController> strongZombies = new List<ZombieController>();
        for (int i = 0; i < ZombieStrongStartAmount; i++) {
            Transform p = zombieStrongSpawnPoints[Random.Range(0, zombieStrongSpawnPoints.Count)];
            zombieStrongSpawnPoints.Remove(p);
            GameObject z = Instantiate(ZombieStrongPrefab, p.position, Quaternion.identity);
            z.transform.SetParent(p);
            strongZombies.Add(z.GetComponent<ZombieController>());
            GameManager.AddActor(z);
        }
        strongZombies[Random.Range(0, strongZombies.Count)].AddKeyToDrop(KeyPrefab);
    }


    // metodo llamado desde GameManager tras haber asesinado
    // a un peaton, que ahora se convierte en zombie
    public void SpawnRandomZombie(Transform otherZombieTr = null) {
        GameObject zombie = ZombiesPrefabs[Random.Range(0, ZombiesPrefabs.Count)];
        if (otherZombieTr == null) {
            Transform parent = spawnPoints[Random.Range(0, spawnPoints.Count)];
            GameObject instance = Instantiate(zombie, parent.position, Quaternion.identity);
            instance.transform.SetParent(parent);
            GameManager.AddActor(instance);
        } else {
            GameObject instance = Instantiate(zombie, otherZombieTr.position, otherZombieTr.rotation);
            instance.transform.SetParent(otherZombieTr.parent);
            GameManager.AddActor(instance);
        }
    }


    /**
     * Cada cierto tiempo se spawnea un zombie normal por punto
     * y un zombie fuerte en un punto aleatorio.
     * Dicho tiempo aleatorio se va decrementando.
     */
    private IEnumerator SpawnZombiesEffect() {
        while (Application.isPlaying) {
            yield return new WaitForSeconds(StartSpawnTime);
            foreach (Transform p in spawnPoints) {
                if (p.childCount < LimitZombiesPerPoint) {
                    GameObject zombie = ZombiesPrefabs[Random.Range(0, ZombiesPrefabs.Count)];
                    GameObject instance = Instantiate(zombie, p.position, Quaternion.identity);
                    instance.transform.SetParent(p);
                    GameManager.AddActor(instance);
                }
            }
            Transform sp = spawnPoints[Random.Range(0, spawnPoints.Count)];
            GameObject z = Instantiate(ZombieStrongPrefab, sp.position, Quaternion.identity);
            z.transform.SetParent(sp);
            StartSpawnTime = Mathf.Max(MinSpawnTime, StartSpawnTime - DecreaseTime);
            GameManager.AddActor(z);
        }
    }

}

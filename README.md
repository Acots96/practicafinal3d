## Práctica Final


*Esta práctica es una ampliación de la PEC3 anterior (ver el README de la PEC3 para conocer más detalles sobre la implementación: https://gitlab.com/Acots96/unjuegodeplataformas3d)*

-

Este es un juego del género horror survival en tercera persona, pero a diferencia de la PEC3 está compuesto por un nivel que contiene una ciudad llena de zombies, gente paseando y coches circulando. No contiene zonas ocultas.

El objetivo principal del juego consiste en matar tantos zombies como puedas para mantener a salvo a los humanos que van paseando por la ciudad.
El juego hace uso de assets como Cinemachine para controlar las cámaras o POLYGONCityPack para la ciudad, entre otros. Personajes y animaciones se han extraído de la web de Mixamo.

El jugador podrá llegar a tener 3 armas, dos de fuego para eliminar enemigos a distancia y una espada para los que estén cuerpo a cuerpo. Inicialmente dispondrá de una pistola, mientras que las dos armas restantes están escondidas por el terreno esperando a ser encontradas. Las teclas que puede usar son:
 - Moverse, saltar y girar: WASD (Mayus para correr), Espacio y ratón.
 - Disparar/Golpe de espada: botón izquierdo del ratón.
 - Apuntar: botón derecho del ratón.
 - Cambiar de arma (teclas numéricas): pistola=1, escopeta=2, espada=3.
 - Recargar: R (solo para armas de fuego).
 - Interactuar: E.
 - Acercar/alejar la cámara: Rueda del ratón.
 - El ratón desaparece y queda bloqueado: pulsar ESC para hacerlo visible y desbloquearlo. Para volver al juego basta con pulsar sobre la pantalla del juego con el botón derecho del ratón o pulsar sobre el botón "Hide Mouse" que aparece en la esquina superior izquierda.
 - Salir al menú principal: Botón "EXIT" en la esquina superior izquierda.
 - El jugador también podrá usar vehículos, donde las teclas son las mismas para moverse (E para entrar y salir), además de poder usar el cláxon con el Espacio.
 
### Menú:
Al iniciar el juego, lo primero que aparece es el menú principal, mediante el cual se puede empezar el juego, abrir el submenú de opciones para modificar aspectos como la resolución de la pantalla o el volumen de la música y los efectos, o finalmente salir del juego.
Si el jugador modifica la resolución de la pantalla, podrá elegir entre varias opciones que serán inferiores a la resolución real para tener el juego en modo ventana, o podrá elegir ponerlo en pantalla completa para adaptar la ventana a la resolución real.
El submenú de opciones está hecho en la misma pantalla, inicialmente escondido hasta que el usuario pasa el ratón por encima de dicho submenú. En ese momento queda visible un menú a la derecha del principal que permite cambiar el volumen de la música y los efectos, la resolución de la pantalla y poner/quitar el juego en pantalla completa, y finalmente (al pasar el ratón por encima) permite ver las teclas que se usan en el juego.

### Terreno:
El terreno está formado por una ciudad con edificios, casas, carreteras y decoración, además de una zona montañosa con una explanada llena de árboles y más decoración. Los zombies están distribuidos por ambas partes.

### Zombies:
El jugador hallará zombies distribuídos por todo el terreno. Están diseñados para deambular y moverse a puntos cercanos a ellos, alternando entre movimiento aleatorio durante un tiempo aleatorio en una dirección aleatoria, y quietos durante un tiempo aleatorio también. También correrán hacia el jugador o hacia los peatones en el caso de que se acerquen lo suficiente como para que los zombies los vean, siempre con prioridad por el jugador.
Para comprobar si hay humanos alrededor (peatones o el jugador), utiliza el método Physics.OverlapSphere 3 veces por segundo únicamente, ya que no es necesario hacerlo a cada frame y no es un método "barato" de hacer habiendo tantos zombies.
Todos los zombies empiezan con la animación de levantarse, para darle realismo tras convertir a un peatón ya que este muere tras caer al suelo, para luego levantarse ya convertido en zombie.

### Peatones:
Distribuidos a lo largo y ancho de la ciudad, hay hasta 6 tipos de peatones que van paseando. Estos utilizan un sistema de nodos para desplazarse, donde cada nodo tiene guardados a sus vecinos, y el peatón elige aleatoriamente entre dichos vecinos para moverse exceptuando el nodo en el que se encuentra y el nodo previo con el objetivo de propiciar un comportamiento más realista, como si el peatón tuviese un destino.
Los peatones se mueven mediante Rigidbody, con un objeto con script ElementsChecker para comprobar los elementos que tenga delante, concretamente los pasos de cebra.
Los peatones también detectan a los zombies de la misma forma que los zombies detectan humanos, por la misma razón de eficiencia. El peatón tiene en cuenta la posición de todos los zombies que está viendo para calcular la dirección en la que debe huir, por lo que si encuentra dos zombies en las direcciones A=(0,0,1) y B=(1,0,0), entonces huirá en la dirección (normalizada) (-A)+(-B)=(-0.7,0,-0.7), que equivaldría a calcular el sentido contrario de la dirección media en la que están los zombies.
Los peatones no empiezan a correr directamente, sino que primero se ejecuta una animación que simula el susto con giro e inicio de correr, por lo que durante esta animación el peatón está de cara a los zombies y, cuando empieza la animación de correr, cambia el sentido de la dirección para huir.
Los zombies tienen una velocidad mayor a la de los peatones, por lo que conseguirán atraparlos la mayoría de las veces. Para que se llegue a dar esta situación, basta con que un zombie esté lo suficientemente cerca del peatón, en ese momento el zombie ejecuta el ataque al peatón, el cual es distinto al ataque que realiza sobre el jugador ya que directamente se avalanza sobre el peatón, y este ejecuta la animación de caer al suelo. Tras unos instantes se destruye el objeto del peatón y se instancia un zombie nuevo en su lugar, que empieza con la animación de levantarse.

### Coches:
También hay 3 tipos de coches circulando por las carreteras. De igual forma que los peatones, los coches también se guían por un sistema de nodos, pero bastante más complejo.
Los vehículos se mueven mediante Rigidbody, con un objeto con script ElementsChecker para comprobar los elementos que tenga delante, concretamente los pasos de cebra y actores no zombies. 
Las carreteras están diseñadas para tener uno o dos carriles en ambas direcciones, empezando y terminando siempre por un cruce donde el coche puede decidir aleatoriamente hacia donde ir, limitándose a los nodos vecinos que tenga el nodo previo al cruce siendo estos unidireccionales, por lo que el coche nunca puede ir hacia atrás, solo hacia uno de los siguientes.
En las carreteras hay 4 tipos de nodos:
 - StartNodes (SN): nodo que encuentra el coche al llegar a un cruce. El coche que llegue a este nodo vendrá desde un FinishNode, y solo se podrá dirigir hacia un TransitionFirstNode.
 - TransitionFirstNode (TFN): nodo desde el que podrá decidir aleatoriamente una de las direcciones permitidas. Tras pasar por un StartNode, el coche llegará al TransitionFirstNode correspondiente, y de aquí decidirá uno de los TransitionLastNode a los que puede ir.
 - TransitionLastNode (TLN): nodo que encuentra el coche tras venir de un TransitionFirstNode, a través del cual pasará hacia el FinishNode correspondiente para finalizar así el paso por el cruce.
 - FinishNode (FN): último nodo que encontrará el coche en el cruce. Este nodo sirve para unir el TransitionLastNode correspondiente con uno de los siguientes StartNode del siguiente cruce.
El esquema de funcionamiento sería el siguiente: 
 - StartNode -> TransitionFirstNode* -> TransitionLastNode -> FinishNode* -> StartNode...
Donde los nodos con un "*" tienen varios nodos siguientes para que el coche elija aleatoriamente (en la mayoría de los casos), puesto que desde un TFN el coche puede elegir dirección (siempre hacia un TLN que vaya en la misma dirección o hacia su izquierda/derecha, nunca en la dirección opuesta) y desde un FN el coche puede elegir uno de los dos SN en el caso de ser una carretera de carril doble. Por otro lado, los SN solo pueden ir hacia un TFN y los TLN también tienen asignado un solo FinishNode.

Como en todo buen GTA, el jugador puede robar coches y conducirlos, para ello solo debe acercarse a cualquier vehículo e interactuar para montarse. Una vez dentro, el personaje se convierte en hijo del transform del coche, se le desactiva el input, el MeshRenderer, el Collider y su Rigidbody pasa a ser kinematic y finalmente se activa la cámara correspondiente (igual que la cámara normal, pero más alejada). Por otro lado, se activa el input del coche para que este se mueva cuando el jugador pulsa las mismas teclas de dirección que controlan al personaje, con la diferencia de que para girar debe estar el coche en movimiento, ya sea hacia atrás o hacia delante, porque sino solo giran las ruedas frontales. Con esto se consigue un movimiento más o menos realista, propio de juegos antiguos.
Para salir del vehículo basta con pulsar la misma tecla de interactuar. Desde ese momento, el vehículo pasará a estar inactivo indefinidamente mientras el jugador no lo vuelva a utilzar.

### Semáforos para el tráfico:
Tal y como se ha mencionado en el apartado anterior, las carreteras están unidas por cruces que permiten a los coches cambiar su rumbo, igual que hay pasos de cebra para que los peatones puedan cambiar de acera, pero para poder gestionar esto sin que sea un caos se han añadido semáforos que se coordinan entre sí para dejar paso a coches y peatones que vayan en una dirección X, y restringir el paso de coches y peatones que se muevan en una dirección Z (siendo X y Z perpendiculares). Cada cierta cantidad de segundos cambia para permitir pasar a los coches y peatones que estaban parados.
Para poder llevar a cabo esto, se han añadido semáforos para coches y para peatones:
 - Los semáforos de los coches son grandes y en forma de L, típicos de las carreteras grandes, y solo hay uno por dirección encima de los nodos TFN de los pasos de cebra. Para que los coches puedan tenerlos en cuenta, los pasos de cebra tienen unos colliders pequeños (donde están posicionados los nodos TFN) que los coches detectan al llegar y "preguntan" al semáforo si está verde para poder pasar. En caso de estar rojo o amarillo, el coche espera hasta que vuelva a estar en verde.
 - Los semáforos para los peatones son más pequeños y verticales, pero a diferencia de los semáforos para los coches, hay dos semáforos para peatones por paso de cebra, uno a cada lado. De la misma forma que los coches, el paso de cebra también tiene un collider para los peatones únicamente, que va de extremo a extremo. Cuando los peatones llegan al paso de cebra también le "preguntan" si está verde para pasar, sino quedan en idle hasta que vuelve a ponerse verde.

### Armas repartidas por el terreno:
El juego mantiene las 3 armas de la PEC anterior, pero ahora el personaje solo posee la pistola al iniciar la partida, ya que las otras 2 armas están repartidas sobre el terreno esperando a ser encontradas por el jugador.
Cuando el jugador encuentre un arma escondida aparecerá el arma guardada como aparecía desde el inicio en la PEC3, además de un mensaje que le indicará qué tecla debe pulsar para usarla (2 para la escopeta y 3 para la espada, independientemente del orden en el que las haya encontrado).

### Atropellar zombies o peatones:
Al conducir un coche el jugador puede atropellar tanto zombies como peatones si estos se encuentran en la parte frontal del vehículo y este supera una mínima velocidad. Los coches no controlados por el jugador también pueden atropellar zombies, y pese a que siempre comprueban si hay peatones delante para detenerse, existe la posiblidad de que atropellen peatones.
El sujeto atropellado explota mediante un efecto de partículas rojo, suficientemente grande como para cubrirlo casi por completo, además de un pequeño efecto de chorros de sangre. Finalmente se instancian unas partes de cuerpo con Rigidbody y se les aplica una fuerza concreta en una dirección aleatoria, que desaparecen tras unos segundos.
 
### Items:
Repartidos por la ciudad hay los mismos items de la práctica anterior (vida y munición), además de dos nuevos items que representan a las dos armas repartidas por el terreno (escopeta y espada).
 
### Sonidos:
Se han mantenido los sonidos de la PEC anterior, añadiendo sonidos para los peatones y los coches. También se han añadido dos AudioMixers, uno para la canción del menú y otro para todos los sonidos de efectos. Los nuevos sonidos añadidos son:
 - Canción y sonidos de los botones del menú.
 - Pasos de los peatones.
 - 6 gritos distintos al ver un zombie.
 - 6 gritos distintos al morir.
 - Coche parado y en marcha. Dependiendo de la velocidad del coche y del sonido que se esté reproduciendo en ese momento, se empieza a reproducir un sonido u otro. Cuando la velocidad es mayor que cero, se reproduce el sonido correspondiente y modifica su pitch acorde con la velocidad, estando siempre entre 0.8 y 1.2, para simular el ruido del motor según la velocidad del vehículo.
 - Choque del coche con un objeto sólido.
 - Cláxon cuando el coche está parado esperando a otro coche o a un peatón. Es un sonido corto que se reproduce cada cierto tiempo aleatorio mientras el coche esté esperando.
 
### Añadidos personales:
 - El coche lleva incorporado un Collider trigger en su parte frontal para evitar chocar con peatones u otros coches y llevárselos por delante. Dicho collider analiza el tag de lo que el coche ha encontrado delante suyo y, en el caso de tratarse de otro coche espera a que este siga su camino. Para evitar que dos coches se queden esperando mútuamente, el primero (A) que ejecuta su OnTriggerEnter se encarga de comprobar si el otro coche (B) ya está esperando a otro coche (C), y si es así entonces (A) se pone a esperar a (B), pero en el caso de (B) no estar esperando a nadie entonces (A) le dice que lo espere a él y (A) sigue su camino, como si (B) estuviese cediendo el paso a (A).
 - Para simular el giro hacia izquierda/derecha desde un TFN hacia un TLN, en runtime se genera un grupo de nodos intermedios cuyas rotaciones son una interpolación entre la rotación del TFN y la del TLN, puestos a lo largo de un arco de unos 90º, para que el coche no vaya directo del TFN al TLN sino que haga el giro lo más realista posible.
 - Para optimizar el juego se ha mantenido el sistema de desactivación de zombies cuya distancia del jugador es superior a un mínimo, añadiendo también a los peatones. También se han reemplazado varios MeshCollider por otros colliders más sencillos como BoxCollider. Finalmente se ha activado el EnableInstancing de los materiales y reducido de 5000 a 750 el FarClipPane de las cámaras.
 
 
### Scripts (nuevos y modificaciones en los existentes):
 - MenuManager (nuevo): script encargado de gestionar las acciones del jugador en el menú principal, como abrir submenús o iniciar/quitar el juego. 
   - Menu (nuevo): script que gestiona el funcionamiento de un menú, usado básicamente para mostrar/ocultar y hacer el efecto de fade con los botones.   
   - MenuButton (nuevo): script encargado de gestionar los efectos de los botones y actualizar la resolucion de la pantalla, entre otros detalles.
 - GameManager (modificado): control de los actores (ahora también peatones, además de los zombies) que aparecen por pantalla, intermediario entre PasserbyController y ZombiesSpawner para convertir un peatón en un zombie y mostrar la información tras encontrar una nueva arma. 
 - Actor (modificado): añadido el método a ejecutar para explotar (declarado virtual ya que no tiene porque explotar un Actor, como el jugador). 
   - PECController (modificado): añadidos los métodos para controlar tanto la entrada como la salida de un vehículo y el cambio de cámara.   
   - ZombieController (modificado): implementación del método de explosión, que instancia un efecto de partículas que simulan la sangre, junto a unas "partes" de cuerpo con Rigidbody que saltan en direcciones aleatorias. Control de la nueva IA que incluye la comprobación de otros actores no zombies a su alrededor.   
   - PasserbyController (nuevo): script que controla el comportamiento de los peatones, el cual incluye la (casi) constante comprobación de zombies alrededor junto con la huída y el sistema de nodos con pasos de cebra. También implementa la interface IElementsChecker.   
 - VehicleController (nuevo): script que controla tanto el comportamiento de los vehículos que circulan por las carreteras como el input cuando el jugador está dentro. También implementa la interface IElementsChecker. 
 - VehiclesSpawner (nuevo): script encargado de instanciar todos los vehículos desde el inicio de la partida. 
 - PassersbySpawner (nuevo): script encargado de instanciar los peatones en nodos aleatorios tanto al principio como cuando muere uno de ellos. 
 - ZombiesSpawner (modificado): añadido un método para instanciar un nuevo zombie donde un peatón haya sido mordido. 
 - ElementsChecker (nuevo): script encargado de implementar el OnTriggerEnter y OnTriggerExit para tener en cuenta unos tags específicos y comunicárselo al objeto especificado, el cual implementa la interface IElementsChecker.  
   - IElementsChecker (nuevo): interface con los métodos ManageColliderEntered(Collider) y ManageColliderExited(Collider) que implementan los objetos que tienen un ElementsChecker, como VehicleController y PasserbyController.   
 - Item (modificado): añadidas las dos nuevas armas como items en el terreno. 
 - Node (nuevo): script que guarda la informacion de un nodo y genera los nodos de transición entre un TFN y un TLN para simular el giro de los vehículos. 
 - TrafficLight (nuevo): script encargado de simular el efecto de un semáforo, activando/desactivando la luz que toque en cada momento, controlado por un TrafficLightsGroup. 
 - TrafficLightsGroup (nuevo): script encargado de controlar todos los TrafficLight de un cruce, alternando entre ambos sentidos (X y Z) y empezando siempre por el sentido Z, por lo que todos los TrafficLightsGroup de la ciudad quedan sincronizados desde el inicio. 
 - Crosswalk (nuevo): script cuya función es recibir la "pregunta" de los vehículos/peatones sobre si el semáforo adjunto está en verde y pueden pasar. 
 - AnimationBehaviours: 
   - ZombieStandupAnimationBehaviour (nuevo): script usado al final de la animación de levantarse para indicar a ZombieController que ya ha concluido dicha animación y puede iniciar el movimiento de wander.   
   - PasserbyRunAnimationBehaviour (nuevo): script usado al principio de la animación de correr para indicar a PasserbyController que debe iniciar el cálculo de la dirección en la que debe huir.
   
### Mejoras posibles:
 - Mejorar el uso de layers y BlendTrees para perfeccionar las animaciones del personaje, puesto que actualmente no tiene el comportamiento 100% esperado en algunas situaciones particulares, como el movimiento del layer superior al correr tras saltar, entre otras.
 - Encontrar más formas de optimizar, como LODs.
 - Mejorar la IA de los coches para que pueda haber más simultaneamente, ya que ahora no tienen el comportamiento esperado cuando se juntan muchos.
 - Añadir obstáculos sólidos en el cálculo de la dirección de huida de los peatones para que no choquen contra las paredes, y modificar su comportamiento tras esquivar a los zombies para evitar que se dirijan a donde estaban y entren en bucle (huir -> volver -> huir...).
 - Añadir más nodos para los peatones y mejorar en general el sistema de wander para que tengan en cuenta a otros peatones.
 - Añadir FSMs a las distintas IAs (zombies, peatones y vehículos) para hacerlas más escalables.

   
*(Ver la implementación del código y los comentarios para más detalles. Todos los elementos añadidos están en Assets/Pec/)*
 
 
Video link: https://youtu.be/gFnH9yPQsss